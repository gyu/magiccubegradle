package com.robotsoft.android.base;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

public class IOManager {
	private final static String SDRoot = "puzzle-game/pictures";

	public static ImageDef download(String urlStr) throws Exception {
		Log.d("RobotSoft","Download "+urlStr);
		Bitmap bmp = null;
		HttpURLConnection con=null;
		InputStream is = null;
		try {
			URL url = new URL(urlStr);
			con = (HttpURLConnection) url.openConnection();
			is = con.getInputStream();
			byte[] bytes=readFully(is);
			bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			return toImageDef(bmp);
		} catch (Exception ex) {
			throw ex;
		} finally {
			if(con!=null) {
				if (is != null) {
					try {
						is.close();
					} catch (Exception ignore) {
					}
				}
				con.disconnect();
			}
		}
	}
	
	private static byte[] readFully(InputStream is) {
		ByteArrayOutputStream out;
		byte buffer[];
		out = null;
		buffer = new byte[2048];
		byte result[];
		try {
			out = new ByteArrayOutputStream();
			int read;
			while ((read = is.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}	
			out.flush();
			result = out.toByteArray();

		} catch (IOException e) {
			result = new byte[0];
		} finally {
			if(out!=null) {
			   try {
				out.close();
			} catch (IOException e) {
			}
			}
		}
		return result;
	}

	public static List<File> getImageFiles() {
		List<File> list = new ArrayList<File>();
		File sdDir = Environment.getExternalStorageDirectory();
		Log.d("RobotSoft","SD dir: "+sdDir.getAbsolutePath());
		if (sdDir.exists() && sdDir.canWrite()) {
			File dir = new File(sdDir, SDRoot);
			if (dir.exists()) {
				for (File f : dir.listFiles()) {
					list.add(f);
				}
			}
		}
		return list;
	}

	public static void storeData(String fileName, byte[] data) {
		File sdDir = Environment.getExternalStorageDirectory();
		if (sdDir == null || !sdDir.exists() || sdDir.canWrite()) {
			Log.i("RobotSoft","SD Dir is not supported.");
			return;
		}
		File dir = new File(sdDir, SDRoot);
		if (!dir.exists()) {
			Log.i("RobotSoft","Create dir "+dir.getAbsolutePath());
			dir.mkdirs();
		}
		File file = new File(dir, fileName);
		FileOutputStream out = null;
		try {
			file.createNewFile();
			if (file.exists() && file.canWrite()) {
				out = new FileOutputStream(file);
				out.write(data);
				Log.d("RobotSoft","Image is saved.");
			}
			else {
				Log.i("RobotSoft","Image cannot be saved.");
			}
		} catch (Exception ex) {
			Log.e("RobotSoft","Failed to write image",ex);

		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception xe) {

				}
			}
		}
	}
	
	public static Bitmap toBitmap(ImageDef def) {
		Bitmap bmp=Bitmap.createBitmap(def.getW(),def.getH(),Bitmap.Config.ARGB_8888);
		bmp.setPixels(def.getData(), 0, def.getW(), 0, 0, def.getW(), def.getH());
		return bmp;
	}
	
	public static ImageDef toImageDef(Bitmap bmp) {
		if(bmp==null) return null;
		int w=bmp.getWidth();
		int h=bmp.getHeight();
		int[] data=new int[w*h];
		bmp.getPixels(data, 0, w, 0, 0, w, h);
		return new ImageDef(w,h,data);
	}

	
	public static void storeImage(ImageDef def,String fileName) throws Exception {
		storeImage(toBitmap(def), fileName);
	}
	
	public static void storeImage(Bitmap bmp,String fileName) throws Exception {
		Log.d("RobotSoft","Store image "+fileName);
		File sdDir = Environment.getExternalStorageDirectory();
		if (sdDir == null || !sdDir.exists() || !sdDir.canWrite()) {
			Log.d("RobotSoft","SD card is not setup: "+sdDir);
			return;
		}	
		File dir = new File(sdDir, SDRoot);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File file = new File(dir, fileName);
		FileOutputStream out = null;
		try {
			file.createNewFile();
			if (file.exists() && file.canWrite()) {
				out = new FileOutputStream(file);
				bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
				Log.d("RobotSoft","Image stored!");
			}
		} catch (Exception ex) {
			Log.e("RobotSoft","Fail to store image",ex);
			throw ex;

		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception xe) {

				}
			}
		}
	}
	
	public static Bitmap findBitmap(String fileName) {
		if(!existInSD(fileName)) return null;
		File file =new File(new File(Environment.getExternalStorageDirectory(), SDRoot),fileName);
		return BitmapFactory.decodeFile(file.getAbsolutePath());
	}
	
	public static boolean existInSD(String fileName) {
		File sdDir = Environment.getExternalStorageDirectory();
		File dir =(sdDir == null || !sdDir.exists() )?null: new File(sdDir, SDRoot);
		File file = (dir!=null && dir.exists())?new File(dir, fileName):null;
		return file!=null && file.exists();
	}

}
