package com.robotsoft.android.base;

public class AbstractStatsItem {
	private int id;
	private int timePlayed;
	
	public AbstractStatsItem(int timePlayed) {
		this(-1, timePlayed);
	}

	public AbstractStatsItem(int id, int timePlayed) {
		this.id = id;
		this.timePlayed = timePlayed;
	}

	public int getId() {
		return id;
	}

	public int getTimePlayed() {
		return timePlayed;
	}

}
