package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.robotsoft.android.magic.R;


public class Cursor {
	public final static int DefState=0;
	public final static int RotState=1;
	public final static int PanState=2;
	public final static int Red=1;
	public final static int Blue=2;
	public final static int Yellow=3;
	public final static int Green=4;
	public final static int Cyan=5;
	public final static int Magenta=6;
	
	
	private static Bitmap defCursor;
	private static Bitmap rotCursor;
	private static Bitmap movCursor;
	private int x;
	private int y;
	private int state=DefState;
	private static int color=DefState;
	
	public Cursor() {
	}
	
	public void paint(Context ctx, Canvas g, int inx, int x,int y, int ox, int oy) {
		int dx=(inx==0)?-5:-16;
		int dy=(inx==0)?-2:-16;
		g.drawBitmap(getBitmap(ctx,inx), x+dx-ox, y+dy-oy, null);
	}

	public void paint(Context ctx, Canvas g, int inx, int x,int y) {
		paint(ctx, g,  inx,  x, y, 0, 0);
	}
	
	public void paint(Context ctx, Canvas g) {
		paint(ctx, g, state, x, y);
	}

	public void paint(Context ctx, Canvas g, int ox, int oy) {
		paint(ctx, g, state, x, y, ox, oy);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public void setColor(int clr) {
		if(clr!=color) {
			color = clr;
			defCursor=null;
			rotCursor=null;
			movCursor=null;
		}
	}
	
	private Bitmap getBitmap(Context ctx,int inx) {
		Bitmap bmp = (inx == DefState) ? defCursor
				: (inx == RotState) ? rotCursor : movCursor;
		if (bmp == null) {
			if (color == DefState) {
				if (inx == DefState) {
					defCursor = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.cursor);
				} else if (inx == RotState) {
					rotCursor = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.cursor_rot);
				} else {
					movCursor = BitmapFactory.decodeResource(ctx.getResources(),R.drawable.cursor_pan);
				}
				return (inx == DefState) ? defCursor
						: (inx == RotState) ? rotCursor : movCursor;
			}
			if (inx == DefState) {
				bmp = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.cursor_black);
			} else if (inx == RotState) {
				bmp = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.cursor_rot);
			} else {
				bmp = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.cursor_pan);
			}	
			int w = bmp.getWidth();
			int h = bmp.getHeight();
			int[] data = new int[w * h];
			bmp.getPixels(data, 0, w, 0, 0, w, h);
			for (int i = 0; i < w * h; i++) {
				int b =data[i] & 0xff;
				if(inx!=PanState) b=255-b;
					int q = data[i] & 0xff000000;
					switch (color) {
					case 1:
						data[i] = q | (b << 16);
						break;
					case 4:
						data[i] = q | (b << 8);
						break;
					case 2:
						data[i] = q | b;
						break;
					case 5:
						data[i] = q | (b << 8) | b;
						break;
					case 6:
						data[i] = q | (b << 16) | b;
						break;
					case 3:
						data[i] = q | (b << 16) | (b << 8);
					}
			}
			bmp = Bitmap.createBitmap(w, h,Bitmap.Config.ARGB_8888);
			bmp.setPixels(data, 0, w, 0, 0, w, h);
			if (inx == DefState)
				defCursor = bmp;
			else if (inx == RotState)
				rotCursor = bmp;
			else
				movCursor = bmp;
		}
		return bmp;
	}	

}
