package com.robotsoft.android.base;

import java.util.Timer;
import java.util.TimerTask;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.View;

import com.robotsoft.android.magic.R;

public class Clock {
	public final static int LeftBottom=3; 
	public final static int TopCenter=7;
	private Timer timer;
	private long startTime;
	private AbstractView parent;
	private Background background;
	private int totalTime = -1;
	private static int orientation=-1;
	
	private Handler handler = new Handler();
	
	
	private Runnable repaint = new Runnable() {
		public void run() {
			parent.invalidate();
		}
	};

	public Clock(AbstractView parent, Background background) {
		super();
		this.parent = parent;
		this.background = background;
	}

	public void start(int time) {
		startTime = System.currentTimeMillis() - time * 1000;
		timer = new Timer();
		timer.schedule(heartBeat(), 1000, 1000);
		totalTime = -1;
	}

	public void paint(Canvas g, int width, Context ctxt) {
		int moves = parent.getMoveNumber();
		Paint paint = new Paint();
		paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
		int height=parent.theHeight();
		int dim=Math.min(width,height);
		paint.setTextSize(dim>=480?18:dim>=320?16:14);
		paint.setAntiAlias(true);
		paint.setColor(ctxt.getResources().getColor(R.color.clock_foreground));
		Rect rect = new Rect();
		String s1= getTimeString(getTime());
		paint.getTextBounds("00:00:00", 0, 8, rect);
		//int w1 = rect.width() + dim>=480?12:dim>=320?10:8;
		//int h = rect.height() + dim>=480?8:dim>=320?7:6;
		int w1 = rect.width() + 12;
		int h = rect.height() + 8;
		int x1;
		int y1;
		int off1=6;
		if(orientation!=-1) {
			if(orientation==LeftBottom) {
				x1=5;
				y1=parent.theHeight()-h-5;
				drawText(g, width, ctxt, paint, s1, x1, y1, w1, h, off1);
				return;
			}
			if(orientation==TopCenter) {
				int border=UIPreference.getBorderSpan();
				x1 = width / 2 - w1 / 2;
				y1 = 4*parent.theHeight()/360 + border;
				drawText(g, width, ctxt, paint, s1, x1, y1, w1, h, off1);
				return;
			}
		}
		String s2 = null;
		int w2 = -1;
		
		int off2=6;
		if (moves != -1) {
			s2 = String.valueOf(moves);
			paint.getTextBounds("0000", 0, 4, rect);
			w2 = rect.width() + 12;
			paint.getTextBounds(s2, 0, s2.length(), rect);
			off2=(w2-rect.width())/2;
		}
		int x2 = -1;
		int y2 = -1;
		
		Rect rct = parent.getBorderBounds();
		if (rct == null) {
			x1 = width / 2 - w1 / 2;
			y1 = 10;
			x2 = width / 2 - w2 / 2;
			y2 = parent.theHeight() - h - 10;
			
		} else if (rct.top > h) {
			if (s2 == null
					|| (rct.bottom < parent.theHeight() - h && (w1 + w2 > rct
							.width() / 2))) {
				x1 = width / 2 - w1 / 2;
				y1 = Math.max(0, rct.top - 6 - h);
				x2 = width / 2 - w2 / 2;
				y2 = Math.min(rct.bottom + 6, parent.theHeight() - h);
			} else {
				y1 = Math.max(0, rct.top - 6 - h);
				y2 = y1;
				if (w1 + w2 < rct.width() / 2) {
					x1 = rct.left;

					x2 = rct.right - w2;

				} else {
					x1 = 15;
					x2 = width - w2 - 15;

				}
			}
		} else if (rct.bottom < parent.theHeight() - h) {
			y1 = Math.min(rct.bottom + 6, parent.theHeight() - h);
			y2 = y1;
			if (s2 == null) {
				x1 = width / 2 - w1 / 2;
			}
			else if (w1 + w2 < rct.width() / 2) {
				x1 = rct.left;
				x2 = rct.right - w2;

			} else {
				x1 = 15;
				x2 = width - w2 - 15;
			}
		} else {
			x1 = width / 2 - w1 / 2;
			y1 = 0;
			x2 = width / 2 - w2 / 2;
			y2 = parent.theHeight() - h;
		}
		drawText(g, width, ctxt, paint, s1, x1, y1, w1, h,off1);
		if (s2 != null) {
			drawText(g, width, ctxt, paint, s2, x2, y2, w2, h,off2);
		}
	}

	private void drawText(Canvas g, int width, Context ctxt, Paint paint,
			String txt, int x, int y, int w, int h, int off) {
		int[] d1 = new int[w * h];
		Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas bg = new Canvas(bmp);
		Paint p = new Paint();
		p.setColor(ctxt.getResources().getColor(R.color.clock_background));
		bg.drawRect(0, 0, w, h, p);
		p = new Paint();
		p.setColor(ctxt.getResources().getColor(R.color.clock_border));
		p.setStyle(Paint.Style.STROKE);
		p.setStrokeWidth(4);
		bg.drawRect(0, 0, w, h, p);
		bg.drawText(txt, off, h / 2 + paint.getFontMetrics().descent + 2, paint);
		bmp.getPixels(d1, 0, w, 0, 0, w, h);
		if(background!=null) {int[] d2 = background.crop(x, y, w, h);
			bmp.setPixels(merge(d1, d2), 0, w, 0, 0, w, h);
		}
		else {
			bmp.setPixels(d1, 0, w, 0, 0, w, h);
		}
		g.drawBitmap(bmp, x, y, null);
	}

	private int[] merge(int[] a1, int[] a2) {
		for (int i = 0; i < a1.length; i++) {
			int r1 = (a1[i] >> 16) & 0xff;
			int r2 = (a2[i] >> 16) & 0xff;
			int g1 = (a1[i] >> 8) & 0xff;
			int g2 = (a2[i] >> 8) & 0xff;
			int b1 = (a1[i]) & 0xff;
			int b2 = (a2[i]) & 0xff;
			a1[i] = 0xff000000 | ((r1 + r2) / 2 << 16) | ((g1 + g2) / 2 << 8)
					| (b1 + b2) / 2;
		}
		return a1;
	}

	private static String str4Int(int i) {
		return (i <= 9 ? "0" : "") + String.valueOf(i);
	}

	public int getTime() {
		if (timer == null) {
			return totalTime == -1 ? 0 : totalTime;
		}
		return (int) ((System.currentTimeMillis() - startTime) / 1000);
	}

	public void stop() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		totalTime = 0;
	}

	private TimerTask heartBeat() {
		return new TimerTask() {
			public void run() {
				handler.post(repaint);
			}
		};
	}

	public void gameDone() {
		stop();
		totalTime = (int) ((System.currentTimeMillis() - startTime) / 1000);
	}

	public static String getTimeString(int t) {
		return str4Int(t / 3600) + ":" + str4Int((t % 3600) / 60) + ":"
				+ str4Int(t % 60);
	}

	public static void setOrientation(int orientation) {
		Clock.orientation = orientation;
	}
	

}
