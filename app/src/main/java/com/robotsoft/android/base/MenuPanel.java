package com.robotsoft.android.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TableLayout;

import com.robotsoft.android.magic.R;

public class MenuPanel extends Activity{
	public static final int NewGame=10;
	public static final int Stats=11;
	public static final int MoreGames=12;
	public static final int Exit=13;
	public static final int Cancel=14;
	public static final int Start=30;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.escape_menu);
        TableLayout table=(TableLayout)findViewById(R.id.escape_menu);
        BackgroundFactory bf=UIPreference.getBackgroundFactory();
        table.setBackgroundDrawable(bf.createFrame(this));
        /*list = (ListView) findViewById(R.id.dialog_list);
        String[] vals=getResources().getStringArray(R.array.options);
        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.dialog_item, vals);
        list.setAdapter(listAdapter);
        list.setSelector(R.drawable.dialog_selector);*/
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void newGame(View v) {
        setResult(NewGame);
        finish();
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void stats(View v) {
    	try {
    		Intent intent = new Intent(this, UIPreference.getStatsClass());
    		startActivityForResult(intent, Stats);
    	}catch(Throwable ex) {
    		Log.e("RobotSoft", "Fail to launch stats panel.", ex);
    	}
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void moreGames(View v) {
    	try {
    		Intent intent = new Intent(this, MoreGamePanel.class);
    		startActivityForResult(intent, MoreGames);
    	}catch(Throwable ex) {
    		Log.e("RobotSoft", "Fail to launch more game panel", ex);
    	}
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void exit(View v) {
        setResult(Exit);
        finish();
    }

    public void cancel(View v) {
        setResult(Cancel);
        finish();
    }
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Stats) {
        }
        else if (requestCode == MoreGames) {
        }
    }
    
}
