package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class DefaultBackgroundFactory implements BackgroundFactory {

	public Drawable createBoard(Context context,HasBorder hasBorder) {
		return new GameBoard(context, hasBorder);
	}
	
	@Override
	public Drawable createFrame(Context context) {
		return new DlgFrame(context, UIPreference.getBackground(),UIPreference.getBorderSpan(), UIPreference.getFrame(),UIPreference.getFrameSpan());
	}
	
	public Drawable createBackground(Context context) {
		return new DefaultBackground(context,UIPreference.getBackground(),UIPreference.getBorderSpan());
	}
	
	public Drawable createFill(Context context) {
		return new FillBackground(context,UIPreference.getBackground(),UIPreference.getBorderSpan());
	}


	static class GameBoard extends Background {
		private HasBorder hasBorder;
		GameBoard(Context context, HasBorder hasBorder) {
			super(context,  UIPreference.getBackground(), UIPreference.getBorderSpan());
			this.hasBorder=hasBorder;
		}

		@Override
		protected Rect borderBounds() {
			return hasBorder.getBorderBounds();
		}
	}
	
	static class FillBackground extends Background {
		
		public FillBackground(Context ctx, int resId, int border) {
			super(ctx, resId, border);
		}

		@Override
		protected Rect borderBounds() {
			return null;
		}

	}



}
