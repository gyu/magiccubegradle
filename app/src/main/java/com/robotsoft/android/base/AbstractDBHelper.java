package com.robotsoft.android.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class AbstractDBHelper  extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	public static final String STATS_TABLE_NAME = "stats";
	public static final String GAME_TABLE_NAME = "game";
	public static final String FIGURE_TABLE_NAME = "figure";
	public static final String LASTUSED_TABLE_NAME = "recent";
	
	public AbstractDBHelper(Context context, String name) {
		super(context, name, null, DATABASE_VERSION);
	}
	
	/*public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		db.execSQL("DROP TABLE IF EXISTS " + STATS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + GAME_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + FIGURE_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + LASTUSED_TABLE_NAME);
		onCreate(db);
	}*/
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL(statsSql(STATS_TABLE_NAME));
		db.execSQL(gameSql(GAME_TABLE_NAME));
		String sql=figureSql(FIGURE_TABLE_NAME);
		if(sql!=null) {
			db.execSQL(sql);
		}
		sql=recentSql(LASTUSED_TABLE_NAME);
		if(sql!=null) {
			db.execSQL(sql);
		}
		ContentValues values = initGame();
		if(values!=null) {
			db.insertOrThrow(GAME_TABLE_NAME, null, values);
		}
	}
	
	protected abstract String statsSql(String table);
	protected abstract String gameSql(String table);
	protected abstract String figureSql(String table);
	protected abstract String recentSql(String table);
	protected abstract ContentValues initGame();
	
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + STATS_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + FIGURE_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + GAME_TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + LASTUSED_TABLE_NAME);
		onCreate(db);
	}
}
