package com.robotsoft.android.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.robotsoft.android.magic.R;

public class HelpPage extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
        WebView view=(WebView)findViewById(R.id.help_page);
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl("file:///android_asset/help.html");
        view.setBackgroundColor(getResources().getColor(R.color.style_background));
    }

}
