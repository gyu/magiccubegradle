package com.robotsoft.android.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.robotsoft.android.magic.R;

public class DownloadPage extends Activity {
	private EditText url;
	private Button download;
	private int state=0;
	private String path;
	private Handler handler;
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		try {
			setContentView(R.layout.download);
			handler=new Handler();
			LinearLayout screen = (LinearLayout) findViewById(R.id.download_page);
			BackgroundFactory bf = UIPreference.getBackgroundFactory();
			screen.setBackgroundDrawable(bf.createBackground(this));
			download=(Button)findViewById(R.id.download_btn);
			url =(EditText) findViewById(R.id.image_url);
			url.setOnKeyListener(new OnKeyListener() {
				public boolean onKey(View view, int keyCode, KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_ENTER&&state==0) {
						doDownload(url);
						return true;
					}
					return false;
				}
			});
			
		} catch (Exception ex) {
			Log.d("RobotSoft", "Download panel failure", ex);
		}
	}
	
	private void startDownload() {
		download.setEnabled(false);
		final String urlStr = url.getText().toString();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Runnable runnable;
				try {
					ImageDef def = IOManager.download(urlStr);
					if (def == null) {
						runnable = new Runnable() {
							@Override
							public void run() {
								Log.d("RobotSoft","Show message");
								download.setEnabled(true);
								Toast toast=Toast.makeText(DownloadPage.this,
										"Failed to download Image", Toast.LENGTH_SHORT);
								toast.show();
							}
						};
					} else {
						Log.d("RobotSoft", "Image downloaded: " + def.getW()
								+ "/" + def.getH());
						state = 1;
						UIPreference.getImageFactory().setCache(def);
						runnable = new Runnable() {
							@Override
							public void run() {
								url.setEnabled(false);
								download.setEnabled(true);
								download.setText("Accept");
								Log.d("RobotSoft", "Start image viewer");
								Intent intent = new Intent(DownloadPage.this, ImageViewer.class);
								startActivityForResult(intent, 0);
							}
						};
					}
				} catch (Exception e) {
					Log.e("RobotSoft", "failed to down load image", e);
					runnable = new Runnable() {
						@Override
						public void run() {
							download.setEnabled(true);
							Toast toast=Toast.makeText(DownloadPage.this,
									"Failed to download Image", Toast.LENGTH_SHORT);
							toast.show();
						}
					};
				}
				Log.d("RobotSoft", "handler start... "+handler);
				handler.post(runnable);
				Log.d("RobotSoft", "handler started");
			}
		};
		new Thread(runnable).start();
	}
	
    @Override
	protected void onActivityResult(int requestCode, int resultCode,
		Intent data) {
    	path=data.getExtras().getString(ImageViewer.PathKey);
    }
    
	public void doDownload(View view) {
    	if(state==0) {
    		startDownload();	
    	}
    	else {
    		Intent intent=new Intent();
    		if(path==null) {
    			path=url.getText().toString();
    		}
    		intent.putExtra(ImageViewer.PathKey, path);
    		setResult(MenuPanel.NewGame, intent);
    		finish();
    	}	
    }

	public void doClose(View view) {
    	UIPreference.getImageFactory().setCache(null);
    	setResult(MenuPanel.NewGame);
		finish();
    }

}
