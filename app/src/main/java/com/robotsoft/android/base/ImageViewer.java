package com.robotsoft.android.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class ImageViewer extends Activity {
	public final static String PathKey="file";
	protected ImagePanel view;
	private int state = 0;
	private boolean original = true;
	private String file;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		view = new ImagePanel(this);
		setContentView(view);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		if(file==null) {
			menu.add(0, Menu.FIRST + 5, 0, "Save to SD");
		}
		menu.add(0, Menu.FIRST + 6, 0, "Close");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int v = item.getItemId() - Menu.FIRST;
		switch (v) {
		case 0:
		case 1:
		case 2:
		case 3:
			this.state = v;
			view.reload();
			return true;
		case 4:
			original = !original;
			view.reload();
			return true;
		case 5:
			save();
			return true;
		case 6:
			done();
			return true;
		}
		return false;
	}
	
	private void done() {
		Intent intent=new Intent();
		intent.putExtra(PathKey, file);
		setResult(0, intent);
		finish();
	}

	private void saveToFile(String fileName) {
		int k=fileName.indexOf('.');
		if(k>0) fileName=fileName.substring(0,k);
		file=fileName+".jpg";
		Bitmap bmp = view.getBitmap();
		try {
			IOManager.storeImage(bmp, file);
		} catch (Exception e) {
			file=null;
			Toast toast=Toast.makeText(this, "Fail to save image", Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	private void save() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Save Image to SD Card");
		alert.setMessage("File name");
		final EditText input = new EditText(this);
		alert.setView(input);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				saveToFile(input.getText().toString());
			}
		});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				});
		alert.show();
	}
	
    @Override
    public void onBackPressed() {
    	Intent intent = new Intent(this, ImageViewerMenu.class);
    	if(state!=0) {
    		intent.putExtra(ImageViewerMenu.RestoreKey, true);
    	}
    	if(!original) {
    		intent.putExtra(ImageViewerMenu.OriginalKey, true);
    	}	
    	else if(view.toFit()) {
    		intent.putExtra(ImageViewerMenu.FitKey, true);
    	}
    	if(file==null) {
    		intent.putExtra(ImageViewerMenu.SaveKey, true);
    	}
        startActivityForResult(intent, 0);
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode,
		Intent data) {
    	switch(resultCode) {
    	case ImageViewerMenu.Restore:
    	case ImageViewerMenu.RotateLeft:
    	case ImageViewerMenu.RotateRight:
    	case ImageViewerMenu.Flip:
    		state=resultCode;
    		view.reload();
    		break;
    	case ImageViewerMenu.Fit:
    	case ImageViewerMenu.Original:
    		original = !original;
			view.reload();
			break;
		case ImageViewerMenu.Save:
			save();
			break;
		case ImageViewerMenu.Close:
			done();
			break;
    	}
    }
	
    
    class ImagePanel extends View {
		protected int width;
		protected int height;
		private ImageDef def;
		private Bitmap bmp;
		private int ox;
		private int oy;
		private int maxox;
		private int maxoy;
		private int startX;
		private int startY;

		public ImagePanel(Context context) {
			super(context);
			setFocusable(true);
			setFocusableInTouchMode(true);
			requestFocus();
			def = UIPreference.getImageFactory().getCache();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			if (bmp == null) {
				bmp = create();
			}
			if(bmp!=null) {
				canvas.drawBitmap(bmp, -ox, -oy, null);
			}
		}
		
		boolean toFit() {
			return (def.getW()>width||def.getH()>height);
		}

		private Bitmap create() {
			int w = def.getW();
			int h = def.getH();
			if (state == 0 && w <= width && h <= height) {
				maxox = 0;
				maxoy = 0;
				ox = (w - width) / 2;
				oy = (h - height) / 2;
				return IOManager.toBitmap(def);
			}
			int nw;
			int nh;
			boolean scaling = false;
			double scaleFactor = 1;
			if (state == 0 || state == 3) {
				nw = w;
				nh = h;
			} else {
				nw = h;
				nh = w;
			}
			if (!original && (nw > width || nh > height)) {
				scaling = true;
				if (nw * height > nh * width) {
					scaleFactor = 1.0 * nw / width;
					nh = (int) (nh * width / nw);
					nw = width;
				} else {
					scaleFactor = 1.0 * nh / height;
					nw = (int) (nw * height / nh);
					nh = height;
				}
			}
			int[] ndata = new int[nw * nh];
			int[] data = def.getData();
			if (!scaling) {
				switch (state) {
				case 2:
					for (int i = 0, k = 0; i < nh; i++) {
						for (int j = 0, l = (nw - 1) * w + i; j < nw; j++, k++, l -= w) {
							ndata[k] = data[l];
						}
					}
					break;
				case 1:
					for (int i = 0, k = 0; i < nh; i++) {
						for (int j = 0, l = nh - 1 - i; j < nw; j++, k++, l += w) {
							ndata[k] = data[l];
						}
					}
					break;
				case 3:
					for (int i = 0, n = nh * nw, j = n - 1; i < n; i++, j--) {
						ndata[i] = data[j];
					}
					break;
				default:
					System.arraycopy(data, 0, ndata, 0, data.length);
					break;
				}
			} else {
				switch (state) {
				case 2:
					for (int i = 0, k = 0; i < nh; i++) {
						int x = Math.min((int) (i * scaleFactor), w - 1);
						for (int j = 0; j < nw; j++, k++) {
							int y = Math.min(
									(int) ((nw - 1 - j) * scaleFactor), h - 1);
							ndata[k] = data[x + y * w];
						}
					}
					break;
				case 1:
					for (int i = 0, k = 0; i < nh; i++) {
						int x = Math.min((int) ((nh - 1 - i) * scaleFactor),
								w - 1);
						for (int j = 0; j < nw; j++, k++) {
							int y = Math.min((int) (j * scaleFactor), h - 1);
							ndata[k] = data[x + y * w];
						}
					}
					break;
				case 3:
					for (int i = 0, k = 0; i < nh; i++) {
						int y = Math.min((int) ((nh - 1 - i) * scaleFactor),
								h - 1);
						for (int j = 0; j < nw; j++, k++) {
							int x = Math.min(
									(int) ((nw - 1 - j) * scaleFactor), w - 1);
							ndata[k] = data[x + y * w];
						}
					}
					break;
				default:
					int k = 0;
					for (int i = 0; i < nh; i++) {
						int y = Math.min((int) (i * scaleFactor), h - 1);
						for (int j = 0; j < nw; j++, k++) {
							int x = Math.min((int) (j * scaleFactor), w - 1);
							ndata[k] = data[x + y * w];
						}
					}
				}
			}
			Bitmap nbmp = Bitmap.createBitmap(nw, nh, Bitmap.Config.ARGB_8888);
			nbmp.setPixels(ndata, 0, nw, 0, 0, nw, nh);
			ox = (nw - width) / 2;
			oy = (nh - height) / 2;
			maxox = (ox > 0) ? nw - width : 0;
			maxoy = (oy > 0) ? nh - height : 0;
			return nbmp;
		}

		public void reload() {
			bmp = create();
			invalidate();
		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			if (width == -1 || width != w || height!=h) {
				initView(w, h);
			}
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			int w = getMeasuredWidth();
			int h = getMeasuredHeight();
			if (width == -1 || width != w || height!=h) {
				initView(w, h);
			}
		}

		private void initView(int w, int h) {
			width = w;
			height = h;
			bmp=null;
			BackgroundFactory bf = UIPreference.getBackgroundFactory();
			setBackgroundDrawable(bf.createFill(getContext()));
		}

		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_DPAD_UP:
				if (maxoy > 0 && move(0, -10))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				if (maxoy > 0 && move(0, 10))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_LEFT:
				if (maxox > 0 && move(-10, 0))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				if (maxox > 0 && move(10, 0))
					return true;
				break;
			}

			return super.onKeyDown(keyCode, event);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			if (maxox > 0 || maxoy > 0) {
				int x = (int) event.getX();
				int y = (int) event.getY();
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startX = x;
					startY = y;
					break;
				case MotionEvent.ACTION_MOVE:
					int dx = maxox == 0 ? 0 : x - startX;
					int dy = maxoy == 0 ? 0 : y - startY;
					if (dx != 0 || dy != 0) {
						if (move(dx, dy)) {
							startX = x;
							startY = y;
						}
					}
					break;
				case MotionEvent.ACTION_UP:
					startX = -1;
					startY = -1;
					break;
				}
				return true;
			}
			return super.onTouchEvent(event);
		}

		private boolean move(int dx, int dy) {
			int a = ox;
			int b = oy;
			if (dx < 0) {
				ox = Math.max(0, dx + ox);
			} else if (dx > 0) {
				ox = Math.min(maxox, dx + ox);
			}
			if (dy < 0) {
				oy = Math.max(0, dy + oy);
			} else if (dy > 0) {
				oy = Math.min(maxoy, dy + oy);
			}
			if (a != ox || b != oy) {
				invalidate();
				return true;
			}
			return false;
		}

		public Bitmap getBitmap() {
			return bmp;
		}
	}

}
