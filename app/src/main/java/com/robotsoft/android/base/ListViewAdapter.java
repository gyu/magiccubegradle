package com.robotsoft.android.base;
import java.util.List;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ListView;

import com.robotsoft.android.magic.R;

public class ListViewAdapter<T> extends ArrayAdapter<T> implements OnItemClickListener {
	private int selected;
	private ListView list;
	private int normal;
	private int highlight;
	
	public ListViewAdapter(Context context, ListView list, 
			int resourceId, T[] objects, int selected) {
		super(context, resourceId, objects);
		this.selected=selected;
		list.setAdapter(this);
		list.setOnItemClickListener(this);
		this.list=list;
		highlight=context.getResources().getColor(R.color.menu_text);
		normal=context.getResources().getColor(R.color.menu_sel_text);
	}
	
	 public View  getView(int position, View convertView, ViewGroup parent) {
		 View v=super.getView(position, convertView, parent);
		 if((v instanceof TextView)&&position==selected) {
			 ((TextView)v).setTextColor(highlight);
		 }
		 else {
			 ((TextView)v).setTextColor(normal);
		 }
		 return v;
     }

	public int getSelected() {
		return selected;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		this.selected = position;
		notifyDataSetChanged();
	}

}
