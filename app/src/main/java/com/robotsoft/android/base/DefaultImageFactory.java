package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class DefaultImageFactory implements ImageFactory {
	private int[] refIds;
	private String path;
	private ImageDef image;
	private int format;
	private int w;
	private int h;
	private ImageDef cache;

	public DefaultImageFactory(int[] refIds) {
		super();
		this.refIds = refIds;
	}

	@Override
	public ImageDef getImage(Context ctx, String path) {
		if(this.path==null||(path!=null&&!this.path.equals(path))||image==null) {
			Bitmap bmp=theBitmap(ctx, path);
			if(bmp==null) return null;
			this.path=path;
			int width = bmp.getWidth();
			int height = bmp.getHeight();
			int[] data = new int[width * height];
			bmp.getPixels(data, 0, width, 0, 0, width, height);
			image=new ImageDef(bmp.getWidth(), bmp.getHeight(), data);
		}
		return image;
	}
	
	@Override
	public ImageDef getImage() {
		return image;
	}
	
	@Override
	public ImageDef getImage(Context ctx, String path, int format, int w, int h) {
		if (image != null) {
			if(this.path.equals(path)&&this.format==format&&this.w==w&&this.h==h) {
				return image;	
			}
		}
		Bitmap bmp = theBitmap(ctx, path);
		if (bmp == null)
			return null;
		this.path=path;
		this.format=format;
		this.h=h;
		this.w=w;
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		int[] data = new int[width * height];
		bmp.getPixels(data, 0, width, 0, 0, width, height);
		if (format == 0 || (width <= w && height <= h)) {
			image = new ImageDef(width, height, data);
		} else if (format == 2) {
			int ww = Math.min(width, w);
			int hh = Math.min(height, h);
			int[] ndata = new int[ww * hh];
			int a = (width - ww) / 2;
			int b = (height - hh) / 2;
			for (int i = 0; i < hh; i++) {
				System.arraycopy(data, (b + i) * width + a, ndata, i * ww, ww);
			}
			image = new ImageDef(ww, hh, ndata);
		} else {
			double scale = Math.min(1.0 * w / width, 1.0 * h / height);
			int ww = (int) (width * scale);
			int hh = (int) (height * scale);
			int[] ndata = new int[ww * hh];
			double factor = 1 / scale;
			for (int i = 0, k = 0; i < hh; i++) {
				int y = Math.min((int) (i * factor), height - 1);
				for (int j = 0; j < ww; j++, k++) {
					int x = Math.min((int) (j * factor), width - 1);
					ndata[k] = data[x + y * width];
				}
			}
			image = new ImageDef(ww, hh, ndata);
		}
		return image;
	}
	
	private Bitmap theBitmap(Context ctx, String path) {
		int index=path.equals("0")?0:path.equals("1")?1:path.equals("2")?2:-1;
		Bitmap bmp;
		if(index>=0) {
			bmp=BitmapFactory.decodeResource(ctx.getResources(), refIds[index]);
		}
		else bmp=IOManager.findBitmap(path);
		if(bmp==null&&cache!=null) {
			bmp=IOManager.toBitmap(cache);
		}
		if(bmp==null) {
			bmp=BitmapFactory.decodeResource(ctx.getResources(), refIds[0]);
		}
		return bmp;
	}

	public ImageDef getCache() {
		return cache;
	}

	public void setCache(ImageDef cache) {
		this.cache = cache;
	}
	
	public void clear() {
		cache=null;
		image=null;
	}

}
