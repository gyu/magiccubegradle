package com.robotsoft.android.base;

import android.graphics.Rect;

public interface HasBorder {
	Rect getBorderBounds();
}
