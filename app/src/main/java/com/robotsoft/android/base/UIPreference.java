package com.robotsoft.android.base;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class UIPreference {
	public static final String Claz="claz";
	public static final String Component="component";
	
	private static int background;
	private static int frame;
	private static int borderSpan;
	private static int frameSpan;
	private static Class newGameClass;
	private static Class statsClass;
	
	private static ImageFactory factory;
	private static BackgroundFactory backgroundFactory;
	private static final float GESTURE_THRESHOLD_DP = 16.0f;

	public static void setBackground(int background) {
		UIPreference.background = background;
	}

	public static int getBackground() {
		return background;
	}

	public static int getFrame() {
		return frame;
	}

	public static void setFrame(int frame) {
		UIPreference.frame = frame;
	}

	public static int getBorderSpan() {
		return borderSpan;
	}

	public static void setBorderSpan(int borderSpan) {
		UIPreference.borderSpan = borderSpan;
	}

	public static int getFrameSpan() {
		return frameSpan;
	}

	public static void setFrameSpan(int frameSpan) {
		UIPreference.frameSpan = frameSpan;
	}

	public static ImageFactory getImageFactory() {
		return factory;
	}

	public static void setImageFactory(ImageFactory factory) {
		UIPreference.factory = factory;
	}

	public static BackgroundFactory getBackgroundFactory() {
		return backgroundFactory;
	}

	public static void setBackgroundFactory(BackgroundFactory backgroundFactory) {
		UIPreference.backgroundFactory = backgroundFactory;
	}


	public static Class getNewGameClass() {
		return newGameClass;
	}

	public static void setNewGameClass(Class newGameClass) {
		UIPreference.newGameClass = newGameClass;
	}

	public static Class getStatsClass() {
		return statsClass;
	}

	public static void setStatsClass(Class statsClass) {
		UIPreference.statsClass = statsClass;
	}
	
	public static int getGestureThreshold(Resources res) {
		float scale = res.getDisplayMetrics().density;
		return(int) (GESTURE_THRESHOLD_DP * scale + 0.5f);
	}

}
