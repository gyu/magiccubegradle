package com.robotsoft.android.base;

import static com.robotsoft.android.base.AbstractDBHelper.STATS_TABLE_NAME;
import android.content.Context;
import android.util.Log;


public abstract class AbstractDatabase {
	protected AbstractDBHelper helper;
	protected AbstractGameData game;
	
	protected abstract AbstractDBHelper create(Context context);
	protected abstract AbstractGameData loadGameData();
	public abstract AbstractStatsItem[] getStats();
	public abstract void gameDone();
	
	public AbstractDatabase(Context context) {
		helper= create(context); 
	}

	public void clearStats() {
		try {
			helper.getWritableDatabase().delete(STATS_TABLE_NAME, null, null);
			Log.d("RobotSoft","Clear Stats");
		}catch(Exception ex) {
			Log.e("RobotSoft","Cannot clear stats",ex);
		}
	}
	
	public AbstractGameData getGameData() {
		if(game==null) {
			try {
				game=loadGameData();
			}catch(Throwable ex) {
				Log.e("RobotSoft","Fail to load Game Data",ex);
			}
		}
		return game;
	}
	
	public void reload() {
		game=loadGameData();
	}

	public void refresh() {
	}

	public abstract void toggleMusic();
	
}
