package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Rect;

public class DefaultBackground extends Background  {
	
	public DefaultBackground(Context ctx, int resId) {
		super(ctx, resId, 0);
	}

	public DefaultBackground(Context ctx, int resId, int border) {
		super(ctx, resId, border);
	}

	@Override
	protected Rect borderBounds() {
		return new Rect(0, 0, width, height);
	}

}
