package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class DlgFrame extends Background {
	private ImageDef[] frameData;

	public DlgFrame(Context ctx, int resId, int border, int frmId, int frm) {
		super(ctx, resId, border);
		Bitmap bmp=BitmapFactory.decodeResource(ctx.getResources(), frmId);
		frameData=frameData(bmp, frm);
	}
	
	protected ImageDef[] frameData() {
		return frameData;
	}

	@Override
	protected Rect borderBounds() {
		return new Rect(0, 0, width, height);
	}
	

}
