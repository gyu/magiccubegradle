package com.robotsoft.android.magic;

import com.robotsoft.android.magic.logic.Matrix;
import com.robotsoft.android.magic.logic.Point;

public class MatrixRot {
	public final static double DefAlpha=5.227347248976912;
	public final static double DefBeta=5.32753140245878;
	private double[][] xyz;
	private double rotA=0;
	private double rotB=0;

	public double[][] getMatrix() {
		return xyz;
	}

	public boolean visible(int k, int a) {
		return xyz[2][k] * a >= 0;
	}

	public double[][] xyArray(int[][] mat) {
		int n = mat[0].length;
		double[][] w = new double[2][n];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				w[i][j] = xyz[i][0] * mat[0][j] + xyz[i][1] * mat[1][j]
						+ xyz[i][2] * mat[2][j];
			}
		}
		return w;
	}

	public double[][] xyArray(double[][] mat) {
		int n = mat[0].length;
		double[][] w = new double[2][n];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				w[i][j] = xyz[i][0] * mat[0][j] + xyz[i][1] * mat[1][j]
						+ xyz[i][2] * mat[2][j];
			}
		}
		return w;
	}

	public double[][] xyzArray(int[][] mat) {
		int n = mat[0].length;
		double[][] w = new double[3][n];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				w[i][j] = xyz[i][0] * mat[0][j] + xyz[i][1] * mat[1][j]
						+ xyz[i][2] * mat[2][j];
			}
		}
		return w;
	}

	public void reset() {
		xyz = new double[3][3];
		xyz[0][0] = 1;
		xyz[1][1] = 1;
		xyz[2][2] = 1;
		rotA=0;
		rotB=0;
	}
	
	private double angle(double a) {
		while(a<0) {
			a+=2*Math.PI;
		}
		while(a>2*Math.PI) {
			a-=2*Math.PI;
		}
		return a;
	}
	
	public double[] rotation(int a, int b) {
		double alpha = a * Math.PI / 360;
		double beta = b * Math.PI / 360;
		return rotation(alpha,beta);
	}

	public double[] rotation(int prime) {
		double ct = Math.cos(DefAlpha);
		double st = Math.sin(DefAlpha);
		double cb = Math.cos(DefBeta);
		double sb = Math.sin(DefBeta);
		double[][] f=new double[3][3];
		f[0][0] = cb;
		f[0][1] = (-st * sb);
		f[0][2] = ct * sb;
		f[1][0] = 0;
		f[1][1] = ct;
		f[1][2] = st;
		f[2][0] = -sb;
		f[2][1] = -st * cb;
		f[2][2] = ct * cb;
		int[][] mat=new int[3][3];
		Point p=Matrix.getPoint(prime);
		int sign=p.getValue();
		if(p.getAxis()==0) {
			mat[0][0]=sign;
			mat[1][1]=sign;
			mat[2][2]=sign;
		}
		else if(p.getAxis()==1) {
			mat[0][1]=sign;
			mat[1][0]=-sign;
			mat[2][2]=sign;
		}
		else  {
			mat[0][2]=sign;
			mat[1][1]=sign;
			mat[2][0]=-sign;
		}
		for(int i=0; i<3; i++) {
			for(int j=0; j<3;j++) {
				double a=0;
				for(int k=0; k<3; k++) {
					a+=mat[i][k]*f[k][j];
				}
				xyz[i][j]=a;
			}
		}
		ct=xyz[1][1];
		st=xyz[1][2];
		cb=xyz[0][0];
		sb=-xyz[2][0];
		rotA = acos(ct);
		if(Math.sin(rotA)*st<0) {
			rotA=Math.PI*2-rotA;
		}
		rotB = acos(cb);
		if(Math.sin(rotB)*sb<0) {
			rotB=Math.PI*2-rotB;
		}
		return new double[]{rotA,rotB};
	}
	
	private double[] rotation(double alpha, double beta) {
		rotA=angle(alpha+rotA);
		rotB=angle(beta+rotB);
		double ct = Math.cos(alpha);
		double st = Math.sin(alpha);
		double cb = Math.cos(beta);
		double sb = Math.sin(beta);
		double Nxx = cb;
		double Nxy = (-st * sb);
		double Nxz = ct * sb;
		double Nyy = ct;
		double Nyz = st;
		double Nzx = (-sb);
		double Nzy = (-st * cb);
		double Nzz = (ct * cb);
		double xx = xyz[0][0];
		double xy = xyz[0][1];
		double xz = xyz[0][2];
		double yx = xyz[1][0];
		double yy = xyz[1][1];
		double yz = xyz[1][2];
		double zx = xyz[2][0];
		double zy = xyz[2][1];
		double zz = xyz[2][2];
		xyz[0][0] = xx * Nxx + yx * Nxy + zx * Nxz;
		xyz[0][1] = xy * Nxx + yy * Nxy + zy * Nxz;
		xyz[0][2] = xz * Nxx + yz * Nxy + zz * Nxz;
		xyz[1][0] = yx * Nyy + zx * Nyz;
		xyz[1][1] = yy * Nyy + zy * Nyz;
		xyz[1][2] = yz * Nyy + zz * Nyz;
		xyz[2][0] = xx * Nzx + yx * Nzy + zx * Nzz;
		xyz[2][1] = xy * Nzx + yy * Nzy + zy * Nzz;
		xyz[2][2] = xz * Nzx + yz * Nzy + zz * Nzz;
		return new double[]{rotA,rotB};
	}

	public double[] initRotation(double alpha, double beta) {
		rotA=angle(alpha);
		rotB=angle(beta);
		double ct = Math.cos(alpha);
		double st = Math.sin(alpha);
		double cb = Math.cos(beta);
		double sb = Math.sin(beta);
		xyz[0][0] = cb;
		xyz[0][1] = -st * sb;
		xyz[0][2] = ct * sb;
		xyz[1][0] =0;
		xyz[1][1] = ct;
		xyz[1][2] = st;
		xyz[2][0] = -sb;
		xyz[2][1] = -st * cb;
		xyz[2][2] = ct * cb;
		return new double[]{rotA,rotB};
	}

	public double getAlpha() {
		return rotA;
	}

	public double getBeta() {
		return rotB;
	}
	

	public static double acos(double x) {
		double f = asin(x);
		if (f == (0.0D / 0.0D))
			return f;
		else
			return 1.5707963267948966D - f;
	}

	public static double asin(double x) {
		if (x < -1D || x > 1.0D)
			return (0.0D / 0.0D);
		if (x == -1D)
			return -1.5707963267948966D;
		if (x == 1.0D)
			return 1.5707963267948966D;
		else
			return atan(x / Math.sqrt(1.0D - x * x));
	}
	
	public static double atan(double x) {
		boolean signChange = false;
		boolean Invert = false;
		int sp = 0;
		if (x < 0.0D) {
			x = -x;
			signChange = true;
		}
		if (x > 1.0D) {
			x = 1.0D / x;
			Invert = true;
		}
		double a;
		for (; x > 0.26179938779914941D; x *= a) {
			sp++;
			a = x + 1.7320508075688772D;
			a = 1.0D / a;
			x *= 1.7320508075688772D;
			x--;
		}

		double x2 = x * x;
		a = x2 + 1.4087812D;
		a = 0.55913709D / a;
		a += 0.60310578999999997D;
		a -= x2 * 0.051604539999999997D;
		a *= x;
		for (; sp > 0; sp--)
			a += 0.52359877559829882D;

		if (Invert)
			a = 1.5707963267948966D - a;
		if (signChange)
			a = -a;
		return a;
	}

}
