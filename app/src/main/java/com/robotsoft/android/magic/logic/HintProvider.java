package com.robotsoft.android.magic.logic;

import java.util.Vector;

import com.robotsoft.android.magic.Renderer;
import com.robotsoft.android.magic.Stone;

public class HintProvider {
	private int[] hist;
	private Renderer model;
	private int rotAxis;
	private int state;
	
	public HintProvider(Renderer model, byte[] vec) {
		this.model = model;
		this.hist = organize(vec);
	}

	
	public int[] getHints() {
		StateCalculator sc=new StateCalculator(model.getStones());
		int current=sc.getState();
		if (hist.length > 0) {
			MoveSupport imp=new MoveSupport(model);
			int max=-1;
			for(int i=0; i<hist.length; i++) {
				Operator op=Matrix.getOperator(hist[i]);
				imp.move(op.getRot(), op.getPos(), !op.getDir());
				StateCalculator cs=new StateCalculator(imp.getStones());
				int state=cs.getState();
				if(state>=current) {
					current=state;
					max=i;
				}
			}
			if(max>=0) {
				int[] res=new int[max+1];
				for(int i=0; i<=max; i++) {
					Operator op=Matrix.getOperator(hist[i]);
					res[i]=Matrix.getAction(op.getRot(), op.getPos(), !op.getDir());
				}
				state=-1;
				return res;
			}
		}		
		state=current+1;
		MoveSupport imp=new  MoveSupport(model);
		Worker worker=new TopLayer(imp, sc.getNorm());
		worker.process(state);
		int[] res=imp.getMoves();
		if(sc.getPrime()==null) {
			rotAxis=(current>=2)?5:0;
			return res;
		}
		Point p=Matrix.getPoint(current>=2?5-sc.getNorm():sc.getNorm());
		for(int i=0; i<3; i++) {
			if(sc.getPrime()[i][p.getAxis()]!=0) {
				rotAxis=Matrix.getOrder(i, sc.getPrime()[i][p.getAxis()]*p.getValue());
				break;
			}
		}
		int[][] mat=Matrix.reverse(sc.getPrime());
		int n = res.length;
		int[] ser=new int[n];
		for(int i=0; i<n; i++) {
			ser[i]=Matrix.transform(res[i], mat);
		}
		return ser;
	}

	public int getState() {
		return state;
	}

	
	private int[] organize(byte[] vec) {
		int n=vec==null?0:vec.length;
		int[] res=new int[n];
		int c=0;
		for(int i=n-1; i>=0; i--) {
			int q=vec[i];
			if(c==0||q==res[c-1]||q%9!=res[c-1]%9) {
				res[c++]=q;
			}
			else {
				c--;
			}
		}
		if(c==n) return res;
		int[] f=new int[c];
		System.arraycopy(res, 0, f, 0, c);
		return f;
	}

	public int getRotAxis() {
		return rotAxis;
	}

}
