package com.robotsoft.android.magic;

import static android.provider.BaseColumns._ID;

import java.util.Random;

import com.robotsoft.android.base.AbstractDBHelper;

import android.content.ContentValues;
import android.content.Context;
import static com.robotsoft.android.magic.DBConstants.*;

public class DBHelper extends AbstractDBHelper {
	private int rnd=1234;
	
	private static final String DATABASE_NAME = "rubik.db";
	public DBHelper(Context context) {
		super(context, DATABASE_NAME);
	}

	@Override
	protected String statsSql(String table) {
		return "CREATE TABLE " + table + " ("
		+ _ID  + " INTEGER PRIMARY KEY AUTOINCREMENT,"
		+ TIME + " INTEGER,"
		+ SHUFFLES + " BLOB,"
		+ OPERATIONS + " BLOB);";   
	}

	@Override
	protected String gameSql(String table) {
		return "CREATE TABLE " + table + " ("
		+ ALPHA + " REAL,"
		+ BETA + " REAL,"
		+ TIME + " INTEGER,"
		+ MUSIC +" INTEGER,"
		+ SHUFFLES + "  BLOB,"
		+ OPERATIONS + "  BLOB);";
	}

	@Override
	protected String figureSql(String table) {
		return null;
	}
	
	private float next() {
		rnd=rnd*1664525+1013904223;
		return ((rnd>>16)&0xffff)/((float)0x10000);
	}
	
	

	@Override
	protected ContentValues initGame() {
		//Random rd = new Random(1000);
		//int n = rd.nextInt(400) + 200;
		//int n =(int)(next()*400) + 200;
		int n=35;
		byte[] v = new byte[n];
		for (int i = 0; i < n; i++) {
			//v[i] =(byte) rd.nextInt(18);
			//v[i] =(byte)( next()*18);
			v[i] =(byte)(i<18?i:i-18);
			
		}
		ContentValues values=new ContentValues();
	    values.put(ALPHA, MatrixRot.DefAlpha);
	    values.put(BETA, MatrixRot.DefBeta);
	    values.put(MUSIC, 0);
	    values.put(SHUFFLES, v);
	    values.put(OPERATIONS, new byte[0]);
	    return values;
	}

	@Override
	protected String recentSql(String table) {
		return null;
	}
	
	

}
