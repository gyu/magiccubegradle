package com.robotsoft.android.magic;

import com.robotsoft.android.base.AbstractGameData;

public class GameData  extends AbstractGameData {
	private double alpha;
	private double beta;
	private byte[] operations;
	private byte[] shuffles;

	public GameData(byte[] shuffles, double alpha, double beta) {
		super();
		this.shuffles = shuffles;
		this.alpha = alpha;
		this.beta = beta;
		operations=new byte[0];
	}
	
	public byte[] getOperations() {
		return operations;
	}
	public void setOperations(byte[] operations) {
		this.operations = operations;
	}
	public double getAlpha() {
		return alpha;
	}
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
	
	public double getBeta() {
		return beta;
	}
	public void setBeta(double beta) {
		this.beta = beta;
	}

	public byte[] getShuffles() {
		return shuffles;
	}

	public void setShuffles(byte[] shuffles) {
		this.shuffles = shuffles;
	}
	
	

}
