package com.robotsoft.android.magic;

import static android.provider.BaseColumns._ID;
import static com.robotsoft.android.base.AbstractDBHelper.GAME_TABLE_NAME;
import static com.robotsoft.android.base.AbstractDBHelper.STATS_TABLE_NAME;
import static com.robotsoft.android.magic.DBConstants.ALPHA;
import static com.robotsoft.android.magic.DBConstants.BETA;
import static com.robotsoft.android.magic.DBConstants.MUSIC;
import static com.robotsoft.android.magic.DBConstants.OPERATIONS;
import static com.robotsoft.android.magic.DBConstants.SHUFFLES;
import static com.robotsoft.android.magic.DBConstants.TIME;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.robotsoft.android.base.AbstractDBHelper;
import com.robotsoft.android.base.AbstractDatabase;
import com.robotsoft.android.base.AbstractGameData;
import com.robotsoft.android.base.AbstractStatsItem;

public class Database extends AbstractDatabase{
	public static int[][] Sizes={{2,3},{3,2},{2,4},{3,4},{4,4}};
	
	public Database(Context context) {
		super(context);
	}

	public int undo() {
		GameData gd=(GameData)getGameData();
		byte[] bs=gd.getOperations();
		int n=bs.length;
		if(n==0) return -1;
		byte[] nbs=new byte[n-1];
		System.arraycopy(bs,0,nbs,0,n-1);
		gd.setOperations(nbs);
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(OPERATIONS, nbs);
		db.update(GAME_TABLE_NAME, values, null, null);
		return (int)bs[n-1];
	}
	
	public AbstractGameData loadGameData() {
		SQLiteDatabase db=helper.getReadableDatabase();
		GameData gd=null;
		Cursor cursor =null;
		try {
		cursor = db.query(GAME_TABLE_NAME, 
				new String[]{ALPHA,BETA,TIME,MUSIC,SHUFFLES,OPERATIONS}, null, null, null,null,null);
		if (cursor.moveToFirst()) {
			gd=new GameData(cursor.getBlob(4),cursor.getDouble(0),cursor.getDouble(1));
			gd.setTimePlayed(cursor.getInt(2));
			gd.setPlayMusic(cursor.getInt(3)>0);
			gd.setOperations(cursor.getBlob(5));
        } 
		
		}catch(Exception ex) {
		}
		finally {
			if(cursor!=null) {
				cursor.close();
			}
		}
		return gd;
	}
	
    public StatsItem[] getStats() {
    	Cursor cursor = helper.getReadableDatabase().query(STATS_TABLE_NAME, 
    			new String[]{_ID, TIME, SHUFFLES, OPERATIONS}, null, null, null,null, TIME);
    	if (cursor == null) {
            return new StatsItem[0];
        } else if (! cursor.moveToFirst()) {
            cursor.close();
            return new StatsItem[0];
        }
    	List<AbstractStatsItem> list=new ArrayList<AbstractStatsItem>();
    	do {
    		StatsItem s=new StatsItem(cursor.getInt(0),cursor.getInt(1),cursor.getBlob(2),cursor.getBlob(3));
    		list.add(s);
    	} while(cursor.moveToNext());
    	cursor.close();
    	return list.toArray(new StatsItem[list.size()]);
    }
    
	public void newMove( int move, int time) {
		GameData gd=(GameData)getGameData();
		byte[] bs=gd.getOperations();
		int n=bs.length;
		byte[] nbs=new byte[n+1];
		System.arraycopy(bs,0,nbs,0,n);
		nbs[n]=(byte)move;
		gd.setTimePlayed(time);
		gd.setOperations(nbs);
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(TIME, time);
		values.put(OPERATIONS, nbs);
		db.update(GAME_TABLE_NAME, values, null, null);
	}
	
	public void reset() {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(TIME,0);
		values.put(OPERATIONS,new byte[0]);
		if(game!=null) {
			((GameData)game).setTimePlayed(0);
			((GameData)game).setOperations(new byte[0]);
		}
		db.update(GAME_TABLE_NAME, values, null, null);
	}
	
	
	public void setPlayMusic(boolean tf) {
		if(game!=null) {
			((GameData)game).setPlayMusic(tf);
		}
		ContentValues values = new ContentValues();
		values.put(MUSIC, tf?1:0);
		SQLiteDatabase db = helper.getWritableDatabase();
		db.update(GAME_TABLE_NAME, values, null, null);
	}
	
	
	public void newGame(GameData game) {
	}
	
	
	private void insertStats(StatsItem item) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(TIME, item.getTimePlayed());
		values.put(SHUFFLES,item.getShuffels());
		values.put(OPERATIONS,item.getOperations());
		db.insertOrThrow(STATS_TABLE_NAME, null, values);
	}
	
		
	private void newStats(StatsItem item) {
		AbstractStatsItem[] items=getStats();
		int n=items==null?0:items.length;
		if(n<10) {
			insertStats(item);
		}
		else if(item.getTimePlayed()>=((StatsItem)items[n-1]).getTimePlayed()) {
			return;
		}
		else {
			SQLiteDatabase db = helper.getWritableDatabase();
		    ContentValues values = new ContentValues();
		    values.put(TIME, item.getTimePlayed());
			values.put(SHUFFLES,item.getShuffels());
			values.put(OPERATIONS,item.getOperations());
			db.update(STATS_TABLE_NAME, values, _ID+"="+items[n-1].getId(), null);
		}
	}

	@Override
	protected AbstractDBHelper create(Context context) {
		return new DBHelper(context);
	}
	
	
	@Override
	public void gameDone() {
		GameData data=(GameData)getGameData();
		StatsItem item=new StatsItem(data.getTimePlayed(),data.getShuffles(),data.getOperations());
		newStats(item);
		reset();
	}
	
	public void refresh() {
		reset();
		Random rd = new Random();
		int n = rd.nextInt(400) + 200;
		byte[] v = new byte[n];
		for (int i = 0; i < n; i++) {
			v[i] = (byte) rd.nextInt(18);
		}
		if (game != null) {
			((GameData) game).setShuffles(v);
		}
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SHUFFLES, v);
		db.update(GAME_TABLE_NAME, values, null, null);
	}

	@Override
	public void toggleMusic() {
		AbstractGameData gd=getGameData();
		boolean b=gd.isPlayMusic();
		gd.setPlayMusic(!b);
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(MUSIC, b?0:1);
		db.update(GAME_TABLE_NAME, values, null, null);
	}

	public void setOrientation(double alpha, double beta) {
		if(game!=null) {
			((GameData)game).setAlpha(alpha);
			((GameData)game).setBeta(beta);
		}
		ContentValues values = new ContentValues();
		values.put(ALPHA, alpha);
		values.put(BETA, beta);
		SQLiteDatabase db = helper.getWritableDatabase();
		db.update(GAME_TABLE_NAME, values, null, null);
		
	}
	
	public StatsItem getStatsItem(int id) {
		StatsItem val=null;
    	Cursor cursor = helper.getReadableDatabase().query(STATS_TABLE_NAME, 
    			new String[]{_ID, TIME, SHUFFLES, OPERATIONS}, _ID+"="+id, null, null,null, null);
    	if (cursor != null) {
    	   if (cursor.moveToFirst()) {
    		   val=new StatsItem(cursor.getInt(0),cursor.getInt(1),cursor.getBlob(2),cursor.getBlob(3));
    	   }
           cursor.close();
        }
    	return val;
	}
}
