package com.robotsoft.android.magic;

import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

public class BackButton extends LightButton {
	private Renderer model;
	public BackButton(Context ctx,int x, int y, int w, int h,Renderer model) {
		super(ctx, x, y, w, h, -1, null);
		this.model=model;
	}
	
	public void paintContent(Canvas g) {
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_text:R.color.menu_text));
		for(int i=-5; i<=5; i++) {
		    g.drawLine(x+4, y+h/2, x+w/2, y+h/2+i,p);	
		}
		g.drawLine(x+w/2, y+h/2, x+w-6, y+h/2,p);
		g.drawLine(x+w/2, y+h/2+1, x+w-6, y+h/2+1,p);
	}
	
	
	
	public int mouseUp() {
		if(selected) {
			model.goBack();
		}
		return -1;
	}

	public boolean in(int a, int b) {
		return (a<x+w+20&&b<y+h+20);
	}
	
}
