package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Renderer;
import com.robotsoft.android.magic.Stone;


public class MoveSupport {
	private int[] moves=new int[500];
	private Stone[] stones;
	private int mark=-1;
	private int current;

	public MoveSupport(Renderer model) {
		super();
		stones=new Stone[26];
		for(int i=0; i<26; i++) {
			stones[i]=new Stone(model.getStones()[i]);
		}
	}
	
	public Stone[] getStones() {
		return stones;
	}

	public void mark() {
		mark=current;
	}
	
	private void doMove(int rot, int pos, boolean dir) {
		int u=rot;
		int v=(u==2)?0:u+1;
	    int w=(u==0)?2:u-1;
	    int[][] m=new int[3][3];
	    m[u][u]=1;
	    m[v][w]=dir?-1:1;
	    m[w][v]=-m[v][w];
	    for (int k = 0; k < 26; k++) {
		    Stone s=stones[k];
		    if(s.valueOf(u)==pos) {
			      s.rotate(m);
		    }
	   }

	}

	public void move(int rot, int pos, boolean dir) {
		doMove(rot,pos, dir);
		int m1=rot*3+pos+1;
		int m2=m1+9;
		int mv=(dir)?m2:m1;
		if(mark==-1&&current>0) {
			int v=moves[current-1];
			if(v!=mv&&(v==m1||v==m2)) {
				current--;
				return;
			}
			
		}
		if(current==moves.length) {
			int[] tmp=moves;
			moves=new int[current+current];
			System.arraycopy(tmp, 0, moves, 0, current);
		}
		moves[current++]=mv;
	}

	public void undo() {
		if(mark!=-1) {
			current=mark;
			mark=-1;
		}
	}

	public int[][][] fetchData() {
		int[][][] data = new int[26][3][3];
		Stone[] ss = getStones();
		for (int i = 0; i < 26; i++) {
			int[][] f = ss[i].getMatrix();
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					data[i][j][k] = f[j][k];
				}
			}

		}
		return data;
	}
	
	
	public void resetData(int[][][] data) {
		Stone[] ss = getStones();
		for (int i = 0; i < 26; i++) {
			int[][] f = new int[3][3];
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					f[j][k] = data[i][j][k];
				}
			}
			ss[i].setMatrix(f);
		}
	}


	
	
	public void unmark() {
		mark=-1;
	}
	
	public int[] getMoves() {
		for(int i=0; i<current; ) {
			if(i>0&&moves[i-1]!=moves[i]&&moves[i-1]%9==moves[i]%9) {
				System.arraycopy(moves, i+1, moves, i-1, current-i-1);
				i--;
				current-=2;
			}
			else {
				i++;
			}
		}
		int[] res=new int[current];
		System.arraycopy(moves, 0, res, 0, current);
		return res;
	}
	
}
