package com.robotsoft.android.magic.logic;

public class Operator {
	private int rot;
	private int pos;
	private boolean dir;
	public Operator(int rot, int pos, boolean dir) {
		super();
		this.rot = rot;
		this.pos = pos;
		this.dir = dir;
	}
	public int getRot() {
		return rot;
	}
	public int getPos() {
		return pos;
	}
	public boolean getDir() {
		return dir;
	}

}
