package com.robotsoft.android.magic;

import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

public class RunButton extends LightButton {
	private LightSpinner[] values;
	private Renderer model;
	
	public RunButton(Context ctx, int x, int y, int w, int h, LightSpinner[] values, Renderer model) {
		super(ctx, x, y, w, h, -1, null);
		this.values=values;
		this.model=model;
	}
	
	
	public void paintContent(Canvas g) {
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_text:R.color.menu_text));
		for(int i=-5; i<=5; i++) {
		    g.drawLine(x+w/2+6, y+h/2, x+w/2-6, y+h/2+i,p);	
		}
	}

	public int mouseUp() {
		if(selected) {
			int v=values[0].getIndex()*3+values[1].getIndex();
			if(values[2].getIndex()>0)v+=9;
			model.startRotation(new int[]{v}, false);
		}
		return -1;
	}
	
	public boolean in(int a, int b) {
		return (a>x-20&&b<y+h+20);
	}

}
