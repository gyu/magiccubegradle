package com.robotsoft.android.magic;

public class Square {
	private int number;
    private double[][] xy;
    
	public Square(int number, double[][] xy) {
		this.number = number;
		this.xy = xy;
	}

	public int getNumber() {
		return number;
	}
	
	public double[][] getXy() {
		return xy;
	}
}
