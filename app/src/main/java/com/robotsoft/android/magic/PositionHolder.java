package com.robotsoft.android.magic;

public class PositionHolder {
	private int[][][][] vals;
	private int face;
	private int row;
	private int col;
	private int count;
	private int f1,r1,c1;
	private int f2,r2,c2;
	
	PositionHolder(double[][][][] v,int offx,int offy, int dim, int w, int h) {
		count=(v[2]!=null)?3:(v[1]!=null)?2:(v[0]!=null)?1:0;
		vals=new int[count][3][3][2];
		int m1=4000000;
		int m2=4000000;
		for(int i=0; i<count; i++) {
			for(int j=0; j<3; j++) {
				for(int k=0; k<3; k++) {
					int x=(int)(v[i][j][k][0]*dim+offx+0.5);
					int y=(int)(v[i][j][k][1]*dim+offy+0.5);
					vals[i][j][k][0]=x;
					vals[i][j][k][1]=y;
					int d=x*x+y*y;
					if(d<m1) {
						m1=d;
						f1=i;
						c1=j;
						r1=k;
					}
					d=(w-x)*(w-x)+(h-y)*(h-y);
					if(d<m2) {
						m2=d;
						f2=i;
						c2=j;
						r2=k;
					}
				}
			}
		}
	}
	
	public int[] getFirst(boolean dir) {
		if (dir) {
			face = f1;
			col=c1;
			row=r1;
		} else {
			face = f2;
			col = c2;
			row = r2;
		}
		return vals[face][col][row];
	}
	
	private boolean sameVector(int a1,int b1,int a2,int b2) {
		if(Math.abs(a1-a2)<3&&Math.abs(b1-b2)<3) return true;
		return (Math.abs(a1+a2)<3&&Math.abs(b1+b2)<3);
	}
	
	private int[] boundary() {
		if(face==f1&& col==c1&&row==r1) {
			return new int[0];
		}
		if(face==f2&&col==c2&&row==r2) {
			return new int[0];
		}
		return null;
	}

    public int[] moved(int dx, int dy) {
		int c = col + dx;
		int r = row + dy;
		if (c >= 0 && c < 3 && r >= 0 && r < 3) {
			col = c;
			row = r;
			return vals[face][col][row];
		}
		if (count == 1)
			return boundary();
		int xx;
		int yy;
		if (dy == 0) {
			xx = vals[face][0][1][0] - vals[face][0][0][0];
			yy = vals[face][0][1][1] - vals[face][0][0][1];
		} else {
			xx = vals[face][1][0][0] - vals[face][0][0][0];
			yy = vals[face][1][0][1] - vals[face][0][0][1];
		}
		int p = -1;
		for (int i = 0; i < count; i++) {
			if (i != face) {
				if (sameVector(vals[i][0][1][0] - vals[i][0][0][0],
						vals[i][0][1][1] - vals[i][0][0][1], xx, yy)
						|| sameVector(vals[i][1][0][0] - vals[i][0][0][0],
								vals[i][1][0][1] - vals[i][0][0][1], xx, yy)) {
					p = i;
					break;
				}
			}
		}
		if (p == -1)
			return boundary();
		int dd = Integer.MAX_VALUE;
		int ii = -1;
		int jj = -1;
		int[] res=null;
		int[] qq=vals[face][col][row];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				int[] q=vals[p][i][j];
				int d2 = Math.abs(q[0] - qq[0])
						+ Math.abs(q[1] - qq[1]);
				if (d2 < dd) {
					dd = d2;
					ii = i;
					jj = j;
					res=q;
				}
			}
		}
		boolean ok=true;
		
		if (dx < 0 && res[0] > qq[0])
			ok=false;
		else if (dx > 0 && res[0] < qq[0])
			ok=false;
		else if (dy < 0 && res[1] > qq[1])
			ok=false;
		else if (dy > 0 && res[1] < qq[1])
			ok=false;
		if(!ok) {
			return boundary();
		}
		face = p;
		col = ii;
		row = jj;
		return res;
	}

	public int getRow() {
		return row;
	}	

	public int getColumn() {
		return col;
	}
	
	public int[] checkPosition(int x,int y) {
 	   for(int i=0; i<count; i++) {
 		   int d00=(vals[i][0][1][0]-vals[i][0][0][0])/2;
 		   int d01=(vals[i][0][1][1]-vals[i][0][0][1])/2;
 		   int d10=(vals[i][1][0][0]-vals[i][0][0][0])/2;
 		   int d11=(vals[i][1][0][1]-vals[i][0][0][1])/2;
 		   double f = 1.0/(d00*d11-d10*d01);
		   for(int j=0; j<3; j++) {
			   for(int k=0; k<3; k++) {
				   int xx=x-vals[i][j][k][0];
				   int yy=y-vals[i][j][k][1];
				   double s = (yy*d00-xx*d01)*f;
				   if(s>-1&&s<1) {
					   s = (xx*d11-yy*d10)*f;
					   if(s>-1&&s<1) {
						   face=i;
						   col=j;
						   row=k;
						   return vals[i][j][k];
					   }
				   }
			   }
		   }   
		}
 	   return null;
	}

}
