package com.robotsoft.android.magic;
import java.util.ArrayList;
import java.util.List;

import android.view.View;

import com.robotsoft.android.base.AbstractView;
import com.robotsoft.android.magic.logic.Matrix;
import com.robotsoft.android.magic.logic.Operator;
import com.robotsoft.android.magic.logic.StateCalculator;


public class Renderer {
	private Stone[] stones;
	private MatrixRot rot = new MatrixRot();
	private CubeFace[] xyz;
	private RubikView view;
	private Animator animator;
	private Database database;
	
	public Renderer(View c, Animator animator, GameData game) {
		this.animator=animator;
		if(c instanceof AbstractView){
			database=(Database)((AbstractView)c).getDatabase();
		}
		if(c instanceof RubikView) {
			this.view=(RubikView)c;	
		}
		animator.setModel(this);
		xyz=new CubeFace[6];
		for(int i=0; i<6; i++) {
			int sign=i<3?1:-1;
			int order=i%3;
			xyz[i]=new CubeFace(rot, order, sign);
		}
		stones = new Stone[26];
		StoneFactory fact=new StoneFactory(); 
		for (int i = -1, n = 0; i <= 1; i++) { 
			for (int j = -1; j <= 1; j++)
				for (int k = -1; k <= 1; k++) {
					if (i != 0 || j != 0 || k != 0) {
						stones[n++] = fact.buildStone(i, j, k, xyz);
					}
				}
		}
		double a = game.getAlpha();
		double b = game.getBeta();
		rot.reset();
		rot.initRotation(a, b);
		byte[] s=new byte[35];
		for(int i=0; i<35; i++)s[i]=(byte)(i<18?i:35-i);
		byte[] t=new byte[0];
		reset(s, t);
		//reset(game.getShuffles(), game.getOperations());
		
		/*StateCalculator sc=new StateCalculator(stones);
		if(sc.getState()!=StateCalculator.AllDone) {
			return;
		}*/
		/*Random rd = new Random();
		int n = rd.nextInt(400) + 200;
		byte[] v = new byte[n];
		for (int i = 0; i < n; i++) {
			v[i] = (byte)rd.nextInt(18);
		}
		reset(v, null);*/
		//PersistService.getInstance().newGame(v, a, b);
	}
	
	
	private void reset(byte[] last, byte[] mvs) {
		if(mvs!=null&&mvs.length>0) {
			int n=last.length;
			int m=mvs.length;
			byte[] d=last;
			last=new byte[n+m];
			System.arraycopy(d, 0, last, 0, n);
			for(int i=0; i<m; i++) {
				last[i+n]=mvs[i];
			}
		}
		reset(last);
	}
	
	public double[][] axisData() {
		double[][] res=new double[3][2];
		double[][] a=rot.getMatrix();
		for(int i=0; i<3; i++)for(int j=0; j<2; j++) res[i][j]=a[j][i];
		return res;
	}
	
	public void rotateMatrix(int alpha, int beta) {
		double[] d=rot.rotation(alpha,beta);
		if(database!=null) {
			database.setOrientation(d[0], d[1]);
		}
	}
	
	public void rotateMatrix(int prime) {
		double[] d=rot.rotation(prime);
		if(database!=null) {
			database.setOrientation(d[0], d[1]);
		}
	}

	public boolean rotateCube(double x,double y, int dx,int dy) {
		CubeFace f=null;
		for (int i=0,n=xyz.length; i<n; i++) {
			if (xyz[i].contains(x/3, y/3)) {
				   f = xyz[i];
				   break;
			}
		}
		if(f==null) {
			return false;
		}
		int rotation = f.findRotation(dx, dy);
		if(rotation!=-1) {
			startRotation(new int[]{rotation}, false);
			return true;
		}
		return false;
	}
	

	public double[][][][] getMiddlePoints() {
		double[][][][] u=new double[3][][][];
		int k=0;
		for(int i=0; i<6; i++) {
			double[][][] s=xyz[i].getMiddlePoints(-1,0, rot.getMatrix());
			if(s!=null) {
				u[k++]=s;
			}
		}
		return u;
	}

	
	public List<Square> getSquares() {
		List<Square> list=animator.getSquares();
		if(list==null) { 
			double[][] mat=rot.getMatrix();
			list=new ArrayList<Square>();
			for(int i=0; i<6; i++) {
				List<Square> res=xyz[i].getSquares(-1,0, mat);
				if(res!=null&&!res.isEmpty()) list.addAll(res);
			}
		}
		return list;
	}
	
	public void reset(byte[] values) {
		for(int i=0; i<26; i++) {
			stones[i].reset();
		}
		makeMoves(values);
	}
	
		
	public void goBack() {
		if(database!=null) {
			int val=database.undo();
			if (val >= 0) {
				Operator op=Matrix.getOperator(val);
				startRotation(new int[]{Matrix.getAction(op.getRot(),op.getPos(),!op.getDir())},true);
			}
		}
	}
	
	public void startRotation(int[] values, boolean update) {
		animator.setSteps(values, update, this);
		if(view!=null) view.startRotation();
	}
	
	private void makeMoves(byte[] moves) {
		for(int i=0; i<moves.length; i++) {
		    int value=moves[i];
		    int dir=-1;
		    if(value>8) {
				dir=1;
				value-=9;
			}
		    doMove(value/3, dir, value%3-1 );
		}   
		fixIndex();
	}
	
	protected void doMove(int u, int dir, int  pos ) {
		int v=(u==2)?0:u+1;
	    int w=(u==0)?2:u-1;
	    int[][] m=new int[3][3];
	    m[u][u]=1;
	    m[v][w]=-dir;
	    m[w][v]=dir;
	    for (int k = 0; k < 26; k++) {
		    Stone s=stones[k];
		    if(s.valueOf(u)==pos) {
			      s.rotate(m);
		    }
	   }
	}
	
	protected void fixIndex() {
		int[][] vals=new int[54][4];
		for(int i=0,k=0; i<26; i++) {
			int[][] b=stones[i].getFaces();
			int[] f=stones[i].getIndexs();
			for(int j=0; j<f.length; j++,k++) {
				for(int d=0; d<3; d++) vals[k][d]=b[j][d];
				vals[k][3]=f[j];
			}
		}
		for(int i=0; i<xyz.length; i++) {
			xyz[i].resetIndex(vals);
		}	
	}
	
	public Stone[] getStones() {
		return stones;
	}


	public CubeFace[] getXyz() {
		return xyz;
	}


	public MatrixRot getMatrixRot() {
		return rot;
	}
	
	
	
}
