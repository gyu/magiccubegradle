package com.robotsoft.android.magic;
import static android.provider.BaseColumns._ID;

import com.robotsoft.android.base.AbstractDatabase;
import com.robotsoft.android.base.AbstractStatsItem;
import com.robotsoft.android.base.AbstractStatsPanel;
import com.robotsoft.android.base.BackgroundFactory;
import com.robotsoft.android.base.Clock;
import com.robotsoft.android.base.UIPreference;
import com.robotsoft.android.magic.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class StatsPanel extends AbstractStatsPanel {	
	
    @Override
	protected void populate(TableLayout table) {
		int n=items==null?0:items.length;
		for(int i=10; i>0; i--) {
        	if(i<=n) {
        		populateRow(i, (StatsItem)items[i-1]);
        		
        	}
        	else {
        		removeRow(table, i);
        	}
        }        
	}

    private void populateRow(int i, StatsItem item) {
    	int timeId;
    	int stepId;
    	switch(i) {
    	case 1:timeId=R.id.time1; stepId=R.id.step1; break;
    	case 2:timeId=R.id.time2; stepId=R.id.step2; break;
    	case 3:timeId=R.id.time3; stepId=R.id.step3; break;
    	case 4:timeId=R.id.time4; stepId=R.id.step4; break;
    	case 5:timeId=R.id.time5; stepId=R.id.step5; break;
    	case 6:timeId=R.id.time6; stepId=R.id.step6; break;
    	case 7:timeId=R.id.time7; stepId=R.id.step7; break;
    	case 8:timeId=R.id.time8; stepId=R.id.step8; break;
    	case 9:timeId=R.id.time9; stepId=R.id.step9; break;
    	default:timeId=R.id.time10; stepId=R.id.step10; break;
    	}
    	TextView tv=(TextView)findViewById(timeId);
    	tv.setText(Clock.getTimeString(item.getTimePlayed()));
    	tv=(TextView)findViewById(stepId);
    	tv.setText(String.valueOf(item.getOperations().length));
	}


	private void removeRow(TableLayout table, int i) {
    	switch(i) {
    	case 1:i=R.id.stats_session_data_1; break;
    	case 2:i=R.id.stats_session_data_2;break;
    	case 3:i=R.id.stats_session_data_3;break;
    	case 4:i=R.id.stats_session_data_4;break;
    	case 5:i=R.id.stats_session_data_5;break;
    	case 6:i=R.id.stats_session_data_6;break;
    	case 7:i=R.id.stats_session_data_7;break;
    	case 8:i=R.id.stats_session_data_8;break;
    	case 9:i=R.id.stats_session_data_9;break;
    	default:i=R.id.stats_session_data_10;
    	}
    	TableRow row=(TableRow)findViewById(i);
    	table.removeView(row);
	}

	@SuppressWarnings({"UnusedDeclaration"})
    public void play1(View v) {
		replay(0);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play2(View v) {
    	replay(1);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play3(View v) {
    	replay(2);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play4(View v) {
    	replay(3);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void play5(View v) {
    	replay(4);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play6(View v) {
    	replay(5);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void play7(View v) {
    	replay(6);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play8(View v) {
    	replay(7);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void play9(View v) {
    	replay(8);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void play10(View v) {
    	replay(9);
    }

	@Override
	protected AbstractDatabase createDatabase() {
		return new Database(this);
	}


	@Override
	protected TableRow content(AbstractStatsItem item) {
		return null;
	}
	
	private void replay(int i) {
		try {
			Intent intent = new Intent(this, ReplayPanel.class);
			intent.putExtra(_ID, items[i].getId());
			startActivity(intent);
		} catch (Exception ex) {
			Log.e("Rubik", "Error in replaying history for (" + i + ")", ex);
		}
	}


}
