package com.robotsoft.android.magic.logic;

public class Point {
	private int axis;
	private int value;
	
	public Point(int axis, int value) {
		this.axis = axis;
		this.value = value;
	}

	public int getAxis() {
		return axis;
	}

	public int getValue() {
		return value;
	}

}
