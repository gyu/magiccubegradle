package com.robotsoft.android.magic;

import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;


public class Parallelogram {
	//private static int[] colors =  {0xffffff00,0xfff80000,0xffffffff,0xff0000f8,0xffff00ff,0xff00f800,0xff142800 };
	private static int[] colors =  {0xffff0000,0xff00ff00,0xff0000ff,0xffffff00,0xffff00ff,0xff00ffff,0xff142800 };
	
	private static int radius;
	private static int lightX;
	private static int lightY;
	private static int ox;
	private static int oy;
	
	private double[][] pnts;
	private int count;
	
	
	
	public static void init(int r,int x,int y) {
		radius=r;
		double a=new Random().nextDouble()*Math.PI*2;
		lightX=(int)(radius*Math.cos(a));
		lightY=(int)(radius*Math.sin(a));
		ox=x;
		oy=y;
	}
	

	public void moveTo(double x, double y) {
		pnts=new double[4][2];
		pnts[0][0]=x;
		pnts[0][1]=y;
		count=1;
	}

	public void lineTo(double x, double y) {
		pnts[count][0]=x;
		pnts[count][1]=y;
		count++;
	}

	public void draw(int[] data, int width,int height) {
		double x=pnts[3][0];
		double y=pnts[3][1];
		for(int i=0; i<4; i++) {
			drawLine(data, width,height,x,y,pnts[i][0],pnts[i][1]);
			x=pnts[i][0];
			y=pnts[i][1];
		}
	}
	

	public void fill(int[] data, int w, int h, int colorIndex) {
		//int[] colors=intArray(color,w);
		int minx = w;
		int miny = h;
		int maxx = 0;
		int maxy = 0;
		for (int i = 0; i < 4; i++) {
			int x=(int)pnts[i][0];
			int y=(int)pnts[i][1];
			if (x < minx)
				minx = x;
			if (x+1 > maxx)
				maxx = x+1;
			if (y < miny)
				miny = y;
			if (y+1 > maxy)
				maxy = y+1;
		}
		if (minx >= w || miny >= h || maxx <= 0 || maxy <= 0)
			return;
		int z1= Math.max(miny, 0);
		int z2 = Math.min(maxy, h);
		if(z2<z1+2) return;
		double[] ep;
		//IColor clr=(colorIndex==6)?null:colors[colorIndex];
		int[] vs=intArray(colors[colorIndex], w);
		for (int i = z1; i < z2; i++) {
			ep = findEndPoints(i);
			if (ep != null && ep[0] < w && ep[1]> 0) {
				int start=(int)ep[0]+1;
				int d =(int)ep[1]-start;
				if(d>2) {
					/*if(clr==null) {
						int[] vs=intArray(0xff142800, d);
				        System.arraycopy(vs, 0, data, start+i*w, d);
					}
				    else {
						for(int j=0,x=start,k=start+i*w; j<d; j++,x++,k++) {
							 int q=108+((x-ox)*lightX+(i-oy)*lightY)*20/(radius*radius);
							 data[k]=clr.color(q);
						}
					}*/
					System.arraycopy(vs, 0, data, start+i*w, d);
				}
			}
		}
	}
	
	
	
	private int[] intArray(int color, int w) {
		int[] d=new int[w];
		d[0]=color;
		doFill(d,1);
		return d;
	}
	
	private void doFill(int[] vals, int l) {
		if(vals.length<=l+l) {
			System.arraycopy(vals, 0, vals, l, vals.length-l);
		}
		else {
			System.arraycopy(vals, 0, vals, l, l);
			doFill(vals,l+l);
		}
	}
	
	

	public double[] findEndPoints(int z) {
		boolean q=false;
		double val=0;
		double x = pnts[3][0];
		double y = pnts[3][1];
		for (int i = 0; i < 4; i++) {
			double a = x;
			double b = y;
			x = pnts[i][0];
			y = pnts[i][1];
			if ((z >= y && z < b) || (z >= b && z < y)) {
				double f = a + (z-b)*(x-a)/(y-b);
				if (!q) {
					val = f;
					q=true;
				} else if(val<f) {
					return new double[] { val, f };
				}
				else {
					return new double[] { f, val };
				}
			}
		}
		return null;
	}
	
	private void drawLine(int[] data,int width, int height, double x1,double y1,double x2,double y2 ) {
		double dx=x2-x1;
		double dy=y2-y1;
		if(Math.abs(dx)<2) {
			if(Math.abs(dy)<3) {
				return;
			}
			drawVertical(data, width, height, (x1+x2)/2, Math.min(y1,y2),Math.max(y1,y2));
		}
		else if(Math.abs(dy)<2) {
			if(Math.abs(dx)<3) {
				return;
			}
			drawHorizontal(data, width, height, (y1+y2)/2, Math.min(x1,x2),Math.max(x1,x2));
		}
		else if(Math.abs(dx)>Math.abs(dy)) {
			scanX(data, width, height, x1<x2?x1:x2, x1<x2?y1:y2, x1<x2?x2:x1, x1<x2?y2:y1);
		}
		else {
			scanY(data, width, height, y1<y2?x1:x2, y1<y2?y1:y2, y1<y2?x2:x1, y1<y2?y2:y1);
		}
	}
	
	private void drawVertical(int[] data,int width, int height,double x,double y1,double y2 ) {
		for(int y=(int)y1+1,k=y*width+(int)x; y<=y2; y++,k+=width) {
			data[k]=0xff000000;
			data[k+1]=0xff000000;
		}
	}

	private void drawHorizontal(int[] data,int width, int height,double y,double x1,double x2 ) {
		for(int x=(int)x1+1,k1=((int)y)*width+x,k2=k1+width; x<=x2; x++,k1++,k2++) {
			data[k1]=0xff000000;
			data[k2]=0xff000000;
		}
	}
	
	private void scanX(int[] data,int width, int height,double x1, double y1,double x2,double y2 ) {
		double k=(y2-y1)/(x2-x1);
		double b=y1-k*x1;
		double f=Math.sqrt(1+k*k)*0.5;
		for(int x=(int)x1+1; x<=x2; x++) {
			y1=x*k+b-f;
			y2=y1+f+f;
			int i1=(int)y1;
			int i2=(int)y2+1;
			for(int y=i1,j=y*width+x; y<=i2; y++,j+=width) {
				data[j]=(y==i1)?colorOf(data[j],y1-i1):(y==i2)?colorOf(data[j],i2-y2):0xff000000;
			}
		}
	}
	
	private void scanY(int[] data,int width, int height, double x1, double y1,double x2,double y2 ) {
		double k=(x2-x1)/(y2-y1);
		double b=x1-k*y1;
		double f=Math.sqrt(1+k*k)*0.5;
		for(int y=(int)y1+1; y<=y2; y++) {
			x1=y*k+b-f;
			x2=x1+f+f;
			int i1=(int)x1;
			int i2=(int)x2+1;
			for(int x=i1,j=y*width+x; x<=i2; x++,j++) {
				data[j]=(x==i1)?colorOf(data[j],x1-i1):(x==i2)?colorOf(data[j],i2-x2):0xff000000;
			}
		}
	}

	
	private int colorOf(int z,double f) {
		int r=(int)(((z>>16)&0xff)*f);
		int g=(int)(((z>>8)&0xff)*f);
		int b=(int)((z&0xff)*f);
		return 0xff000000|r<<16|g<<8|b;
	}
}
