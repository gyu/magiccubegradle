package com.robotsoft.android.magic;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.robotsoft.android.magic.logic.Matrix;
import com.robotsoft.android.magic.logic.Operator;

public class Animator {
	private static int Period=6;
	private static int Loop=Period+Period/2;
	private int[] steps;
	private int[] data;
	private int[] backData;
	private boolean undo;
	private boolean update;
	private double[][] mat;
	private CubeFace[] xyz;
	private List<Square> squares;
	private Renderer model;
	private boolean populated;
	private int count;
	
	private boolean paused;

	public Animator(boolean update) {
		this.update = update;
	}

	public void setModel(Renderer model) {
		this.model = model;
	}

	public void setSteps(int[] steps, boolean undo,Renderer model) {
		count=0;
		this.data=steps;
		this.steps = steps;
		backData=new int[data.length+1]; 
		this.undo = undo;
		this.xyz = model.getXyz();
		this.mat = model.getMatrixRot().getMatrix();
	}

	public int getTotalSteps() {
		return (steps.length-1)*Loop+Period;
	}

	public int getState() {
		if(paused&&steps!=backData) return 3;
		count++;
		if(steps!=backData&&count==getTotalSteps()) return 2;
		int m = count % Loop;
		if(m==Period) {
			if(steps==backData) {
				steps=data;
				count=Math.max(0, count-2*Loop);
				return 4;
			}
			return 1;
		}
		return (m == 0 || m > Period) ? -1 : 0;
	}

	public boolean doStore() {
		return update && !undo;
	}

	public int getMove() {
		return steps[count/Loop];
	}

	public void setIdle() {
		squares = null;
	}

	public List<Square> getSquares() {
		return squares;
	}
	
	public void pause() {
		paused=true;
	}
	
	public void resume() {
		if(paused) {
			paused=false;
		}
	}

	public void restart() {
		paused=false;
		count=0;
	}
	
	
	
	public boolean isPaused() {
		return paused;
	}

	public boolean goBack() {
		if(count>=Period) {
			int step=count/Loop;
			System.arraycopy(data, 0, backData, 0, step+1);
			System.arraycopy(data, step+1, backData, step+2, data.length-step-1);
			Operator op=Matrix.getOperator(data[step]);
			backData[step+1]=Matrix.getAction(op.getRot(),op.getPos(),!op.getDir());
			steps=backData;
			count=(step+1)*Loop-1;
			return true;
		}
		return false;
	}


	public void prepareDraw() {
		Operator pnt = Matrix.getOperator(steps[count / Loop]);
		double alpha = Math.PI * 0.5 * (count % Loop) / Period;
		double ca = Math.cos(alpha);
		double sa = Math.sin(alpha);
		double[][] res = new double[3][3];
		int axis = pnt.getRot();
		res[axis][axis] = 1;
		int u = axis == 2 ? 0 : axis + 1;
		int v = axis == 0 ? 2 : axis - 1;
		res[u][u] = ca;
		res[v][v] = ca;
		int sgn = pnt.getDir() ? 1 : -1;
		res[u][v] = -sa * sgn;
		res[v][u] = sa * sgn;
		double[][] rotMat = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++)
				rotMat[i][j] = mat[i][0] * res[0][j] + mat[i][1] * res[1][j]
						+ mat[i][2] * res[2][j];
		}
		squares = new ArrayList<Square>();
		int a = pnt.getRot();
		int p = pnt.getPos();
		if (a >= 0) {
			boolean b = (mat[2][a] > 0);
			for (int i = -1; i <= 1; i++) {
				int j = b ? i : -i;
				pickFace(squares, a, j, j == p ? rotMat : mat);
				squares.add(new Square(-1, null));
			}
		}

		if (count % Loop == 1 && populated) {
			populated = false;
		} else if (count % Loop == Period-1 && !populated) {
			populated = true;
			model.doMove(pnt.getRot(), pnt.getDir() ? 1 : -1, pnt.getPos());
			model.fixIndex();
		}
	}

	private void pickFace(List<Square> list, int axis, int position, double[][] matrix) {
		for (int i = 0; i < 6; i++) {
			List<Square> res=xyz[i].getSquares(axis, position, matrix);
			if(res!=null&&!res.isEmpty()) list.addAll(res);
		}
		if (axis >= 0) {
			if (position == 0) {
				int k = (matrix[2][axis] > 0) ? 1 : -1;
				list.add(xyz[axis].middlePlane(k, matrix));
			} else {
				if ((position == -1 && matrix[2][axis] > 0)
						|| (position == 1 && matrix[2][axis] < 0))
					list.add(xyz[axis].middlePlane(position, matrix));
			}
		}
	}

}
