package com.robotsoft.android.magic;

import com.robotsoft.android.base.AbstractView;
import com.robotsoft.android.base.Clock;
import com.robotsoft.android.base.DefaultBackgroundFactory;
import com.robotsoft.android.base.Game;
import com.robotsoft.android.base.UIPreference;
import com.robotsoft.android.magic.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TableLayout;

public class Rubik extends Game {

	@Override
	protected void mapUIPreference() {
		UIPreference.setBorderSpan(0);
    	UIPreference.setFrameSpan(10);
    	UIPreference.setBackground(R.drawable.background);
    	UIPreference.setFrame(R.drawable.frame);
    	UIPreference.setBackgroundFactory(new DefaultBackgroundFactory());
    	UIPreference.setStatsClass(StatsPanel.class);
    	Clock.setOrientation(Clock.LeftBottom);
	}

	@Override
	protected AbstractView createView() {
		return new RubikView(this);
	}

	@Override
	protected void processOption(int resultCode, Intent data) {
	}

	@Override
	protected void processBundle(Bundle bundle) {
	}
	
	@Override
	protected void showNewGameMenu() {
		view.start();
	}
    
}