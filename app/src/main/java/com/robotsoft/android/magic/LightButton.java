package com.robotsoft.android.magic;

import com.robotsoft.android.base.Cursor;
import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class LightButton implements Focusable {
	protected int actionId;
	protected boolean selected;
	protected int x;
	protected int y;
	protected int w;
	protected int h;
	protected String name;
	protected Context context;
	
	public LightButton(Context context,int x, int y, int w, int h, int actionId, String name) {
		this.context=context;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.name = name;
		h = ((h + 1) / 2) * 2;
		this.actionId = actionId;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int mouseUp() {
		if (selected)
			return actionId;
		return -1;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int paintContent(Canvas g, int x,int y, int w,int h, String s) {
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_text:R.color.menu_text));
		p.setTextSize(h*3/4);
		p.setAntiAlias(true);
		Rect bounds=new Rect();
		p.getTextBounds(s, 0, s.length(), bounds);
		x=x+w/2-bounds.left-bounds.width()/2;
		y=y+h/2-bounds.top-bounds.height()/2;
		g.drawText(s,x,y,p);
		return x+bounds.width()/2;
	}
	
	public void paintContent(Canvas g) {
		paintContent(g, x, y, w, h,  name);
	}
	
	public void paintBox(Canvas g) {
		paintBox(g, x,y,w, h);
	}
	
	
	public void paint(Canvas g) {
		paintBox(g);
		paintContent(g);
	}
	
	public void paint(Canvas g, int ox,int oy) {
		paintBox(g, x-ox, y-oy, w, h);
		paintContent(g, x-ox, y-oy, w, h,  name);
	}

	
	
	public void paintBox(Canvas g, int x, int y, int w, int h) {
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_border:R.color.menu_border));
		g.drawRect(x-1, y-1, x+w+1, y+h+1, p);
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_background:R.color.menu_background));
		g.drawRect(x+1, y+1, x+w-1, y+h-1, p);
	}

	public boolean in(int a, int b, int range) {
		return a >= x-range && a < x + w+range && b >= y-range-20 && b < y + h+range+20;
	}

	public boolean isDown() {
		return selected;
	}

	@Override
	public boolean lostFocus(int dx, int dy) {
		if(selected) {
			selected=false;
			return true;
		}
		return false;
	}

	@Override
	public boolean gainFocus(boolean dir, int index) {
		if(!selected) {
			selected=true;
		}
		return true;
	}

	@Override
	public boolean hasFocus() {
		return selected;
	}

}
