package com.robotsoft.android.magic;

import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class MessagePane {
	private static String[] Words={"Cross in Bottom Layer","Complete Bottom Layer",
		"Complete Middle Layer","Cross in Top Layer","Fix Top Layer","Corners in Top Layer","Fix 4 Edges"};

	private Context context;
	private String message;
	private int width;
	private int height;
	
	public MessagePane(Context context, String message, int width, int height) {
		this.context = context;
		this.message =message;
		this.width = width;
		this.height = height;
	}

	
	public MessagePane(Context context, int state, int width, int height) {
		this(context, Words[state<0?0:state>6?6:state], width, height);
	}
	
	public void paint(Canvas g) {
		Paint txt=new Paint();
		txt.setTextSize(24);
		Rect rct=new Rect();
		txt.getTextBounds(message, 0, message.length(), rct);
		if(rct.width()>width-20) {
			int k=message.lastIndexOf(' ');
			paintTwoLine(g, message.substring(0,k), message.substring(k+1));
			return;
		}
		txt.setAntiAlias(true);
		int w=Math.min(width, rct.width()+30);
		int h=rct.height()+28;
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(R.color.msg_border));
		g.drawRect(width/2-w/2, height-h,width/2+w/2,height,p);
		p.setColor(context.getResources().getColor(R.color.msg_background));
		g.drawRect(width/2-w/2+2, height-h+2,width/2+w/2-2,height-2,p);
		g.drawText(message, width/2-rct.left-rct.width()/2, height-14-rct.top-rct.height(), txt);
	}

	private void paintTwoLine(Canvas g, String line1, String line2) {
		Paint txt=new Paint();
		txt.setTextSize(24);
		txt.setAntiAlias(true);
		Rect rct1=new Rect();
		txt.getTextBounds(line1, 0, line1.length(), rct1);
		Rect rct2=new Rect();
		txt.getTextBounds(line2, 0, line2.length(), rct2);
		int w=Math.min(rct1.width(), rct2.width())+30;
		int h=rct1.height()*2+28+10;
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(R.color.msg_border));
		g.drawRect(width/2-w/2, height-h,width/2+w/2,height,p);
		p.setColor(context.getResources().getColor(R.color.msg_background));
		g.drawRect(width/2-w/2+2, height-h+2,width/2+w/2-2,height-2,p);
		g.drawText(line1, width/2-rct1.left-rct1.width()/2, height-24-rct1.top-rct1.height()-rct2.height(), txt);
		g.drawText(line2, width/2-rct2.left-rct2.width()/2, height-14-rct2.top-rct2.height(), txt);
	}
	

}
