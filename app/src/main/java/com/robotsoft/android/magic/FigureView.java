package com.robotsoft.android.magic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.robotsoft.android.base.HasBorder;
import com.robotsoft.android.magic.ReplayPanel.ProgressListener;


public class FigureView extends View implements HasBorder {
	private int width;
	private int height;
	private RubikBody body;
	private Animator animator;
	private byte[] shuffles;
	private byte[] operations;
	private ProgressListener listener;
	
	public FigureView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public FigureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FigureView(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		animator=new Animator(false);
		setFocusable(true);
		setFocusableInTouchMode(true);
		requestFocus();
	}


	@Override
	public Rect getBorderBounds() {
		return null;
	}

	public void resume() {
		if(animator.isPaused()) {
			animator.resume();
			start();
		}
	}

	public void restart() {
		if(animator.isPaused()) {
			animator.restart();
			start();
		}
	}

	public boolean goBack() {
		if(animator.isPaused()) {
			Log.d("Rubik", ">>>  Goback");
			if(animator.goBack()){
				Log.d("Rubik", ">>>  Do Start");
				start();
				return true;
			}
		}
		return false;
	}
	
	public void pause() {
		animator.pause();
	}

	public void setStatsItem(StatsItem statsItem,
		ProgressListener progressListener) {
		
		shuffles=statsItem.getShuffels();
		if(shuffles==null) {
			Log.e("Rubik","Corrupt stats data.");
		}
		else Log.d("Rubik","Data: "+shuffles.length); 
		operations = statsItem.getOperations();
		listener=progressListener;
		if(body==null) {
			doInit();
		}
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    int w = getMeasuredWidth();
		int h = getMeasuredHeight()-50;
		setMeasuredDimension(w, h);
		if (width == -1 || width != w) {
			initView(w, h);
		}
	}

	private void initView(int w, int h) {
		width = w;
		height = h;
		if(shuffles!=null) {
			doInit();
		}
	}
	
	private void doInit() {
		Database db=new Database(getContext());
		GameData d=(GameData)db.getGameData();
		d.setShuffles(shuffles);
		d.setOperations(new byte[0]);
		body = new RubikBody(this, animator, d);
		body.setDimension(width, height);
		int[] a=new int[operations.length];
		for(int i=0; i<a.length; i++) {
			a[i]=operations[i];
		}
		animator.setSteps(a, false, body.getModel());
		start();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if(body!=null) {
			body.paint(canvas);
		}
	}
	
	private void start() {
		Log.d("Rubik", ">>> Start to  Goback "+animator.getTotalSteps());
		Fly fly = new Fly(listener);
		fly.setRepeatCount(animator.getTotalSteps());
		fly.setDuration(60);
		startAnimation(fly);
	}
	
	private class Fly extends Animation {
		private ProgressListener listener;
		
		public Fly(ProgressListener listener) {
			this.listener = listener;
		}

		@Override
		protected void applyTransformation(float interpolatedTime,
				Transformation t) {
			int state = animator.getState();
			Log.d("Rubik", ">>> Get state "+state);
			if(state==3) {
				listener.onBack();
				clearAnimation();
			}
			else if (state <= 0) {
				if (state == 0) animator.prepareDraw();
				else animator.setIdle();
			}
			else {
				animator.setIdle();
				if (state >= 2) {
					listener.onBack();
					clearAnimation();
				}
			}
		}
	}


}
