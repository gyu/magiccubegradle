package com.robotsoft.android.magic;

public class StoneFactory {

    public Stone buildStone(int x,int y,int z, CubeFace[] cubes) {
    	int[] xyz=new int[3];
        xyz[0] = x;
        xyz[1] = y;
        xyz[2] = z;
        int type=3;
        if(x==0) type--;
        if(y==0) type--;
        if(z==0) type--;
        int[][] vals=new int[type][];
        int[] index=new int[type];
        int n=0;
        for(int i=0; i<cubes.length; i++) {
        	int[] f=cubes[i].getFace(x, y, z);
        	if(f!=null) {
        		index[n]=cubes[i].getIndex();
        		vals[n]=f;
        		n++;
        		if(n==type) break;
        	}
        }
        return new Stone(xyz, vals, index);
    }

}
