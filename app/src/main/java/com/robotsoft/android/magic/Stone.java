package com.robotsoft.android.magic;

public class Stone {
	private int[][] matrix;
	private int[] xyz;
	private int[][] faces;
	private int[] indexs;
	private int hint;

	public Stone(int[] xyz, int[][] faces, int[] colors) {
		this.xyz = xyz;
		this.faces = faces;
		this.indexs=colors;
		reset();
	}

	public Stone(Stone s) {
		xyz=new int[3];
		System.arraycopy(s.xyz, 0, xyz, 0, 3);
		int n=s.indexs.length;
		indexs=new int[n];
		System.arraycopy(s.indexs, 0, indexs, 0, n);
		matrix=new int[3][3];
		for(int i=0; i<3; i++) {
			System.arraycopy(s.matrix[i], 0, matrix[i], 0, 3);
		}
		faces=new int[n][3];
		for(int i=0; i<n; i++) {
			System.arraycopy(s.faces[i], 0, faces[i], 0, 3);
		}
	}

	public void reset() {
		matrix = new int[3][3];
		matrix[0][0] = 1;
		matrix[1][1] = 1;
		matrix[2][2] = 1;
	}

	public int[] coordinate() {
		int[] res = new int[3];
		res[0] = matrix[0][0] * xyz[0] + matrix[0][1] * xyz[1] + matrix[0][2]
				* xyz[2];
		res[1] = matrix[1][0] * xyz[0] + matrix[1][1] * xyz[1] + matrix[1][2]
				* xyz[2];
		res[2] = matrix[2][0] * xyz[0] + matrix[2][1] * xyz[1] + matrix[2][2]
				* xyz[2];
		return res;
	}

	public int valueOf(int k) {
		return matrix[k][0] * xyz[0] + matrix[k][1] * xyz[1] + matrix[k][2] * xyz[2];
	}

	boolean match(int[] t) {
		if (t[0] != matrix[0][0] * xyz[0] + matrix[0][1] * xyz[1]
				+ matrix[0][2] * xyz[2])
			return false;
		if (t[1] != matrix[1][0] * xyz[0] + matrix[1][1] * xyz[1]
				+ matrix[1][2] * xyz[2])
			return false;
		return t[2] == matrix[2][0] * xyz[0] + matrix[2][1] * xyz[1]
				+ matrix[2][2] * xyz[2];
	}

	int normal(int t) {
		return matrix[0][t] != 0 ? 0 : matrix[1][t] != 0 ? 1 : 2;
	}

	public void rotate(int[][] t) {
		int xx = t[0][0] * matrix[0][0] + t[0][1] * matrix[1][0] + t[0][2]
				* matrix[2][0];
		int xy = t[0][0] * matrix[0][1] + t[0][1] * matrix[1][1] + t[0][2]
				* matrix[2][1];
		int xz = t[0][0] * matrix[0][2] + t[0][1] * matrix[1][2] + t[0][2]
				* matrix[2][2];
		int yx = t[1][0] * matrix[0][0] + t[1][1] * matrix[1][0] + t[1][2]
				* matrix[2][0];
		int yy = t[1][0] * matrix[0][1] + t[1][1] * matrix[1][1] + t[1][2]
				* matrix[2][1];
		int yz = t[1][0] * matrix[0][2] + t[1][1] * matrix[1][2] + t[1][2]
				* matrix[2][2];
		int zx = t[2][0] * matrix[0][0] + t[2][1] * matrix[1][0] + t[2][2]
				* matrix[2][0];
		int zy = t[2][0] * matrix[0][1] + t[2][1] * matrix[1][1] + t[2][2]
				* matrix[2][1];
		int zz = t[2][0] * matrix[0][2] + t[2][1] * matrix[1][2] + t[2][2]
				* matrix[2][2];
		matrix[0][0] = xx;
		matrix[0][1] = xy;
		matrix[0][2] = xz;
		matrix[1][0] = yx;
		matrix[1][1] = yy;
		matrix[1][2] = yz;
		matrix[2][0] = zx;
		matrix[2][1] = zy;
		matrix[2][2] = zz;
	}
	
	public void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}

	public int[][] getFaces() {
		int n=faces.length;
		int[][] res = new int[n][3];
		for (int i = 0; i < n; i++) {
				int[] k = faces[i];
				int[] d = new int[3];
				for (int l = 0; l < 3; l++) {
					d[l] = matrix[l][0] * k[0] + matrix[l][1] * k[1]
							+ matrix[l][2] * k[2];
				}
				res[i] = d;
		}
		return res;
	}

	public int[][] getMatrix() {
		return matrix;
	}

	public int getType() {
		return indexs.length;
	}

	public int[] getData() {
		return xyz;
	}

	public int[] getIndexs() {
		return indexs;
	}

	public int getHint() {
		return hint;
	}

	public void setHint(int hint) {
		this.hint = hint;
	}
	
}
