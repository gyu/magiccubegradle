package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;



public abstract class Above extends Worker {
	protected static int[][][] Cubes = {
		{ { 1, 2 }, { 2, 4 }, { 4, 3 }, { 3, 1 } },
		{ { 0, 3 }, { 3, 5 }, { 5, 2 }, { 2, 0 } },
		{ { 0, 1 }, { 1, 5 }, { 5, 4 }, { 4, 0 } },
		{ { 0, 4 }, { 4, 5 }, { 5, 1 }, { 1, 0 } },
		{ { 0, 2 }, { 2, 5 }, { 5, 3 }, { 3, 0 } },
		{ { 1, 3 }, { 3, 4 }, { 4, 2 }, { 2, 1 } } };
	
	protected int root;
	protected int xa, ya, za;
	protected int xv, yv, zv;
	protected int right;
	protected int face;
	
	public Above(MoveSupport host, int root) {
		super(host);
		this.root = root;
		Point pnt=Matrix.getPoint(root);
		za=pnt.getAxis();
		zv= pnt.getValue();
	}
	
	protected void fix() {
		Point pnt=Matrix.getPoint(face);
		xa=pnt.getAxis();
		xv=pnt.getValue();
		pnt=Matrix.getPoint(right);
		ya=pnt.getAxis();
		yv=pnt.getValue();
	}
	
	protected Stone findStone(int a1, int v1, int a2, int v2) {
		Stone[] stones = host.getStones();
		for (int i = 0; i < stones.length; i++) {
			if (stones[i].getType() == 2) {
				int[] w=stones[i].coordinate();
				if(w[a1]==v1&&w[a2]==v2) {
					return stones[i];
				}
			}
		}
		return null;
	}
	
	
	private void rotY(boolean dir) {
		Stone s=findStone(za,-zv,ya,yv);
		move(ya,yv,true);
		if((s.coordinate()[xa]==xv)!=dir) {
			move(ya,yv,false);
			move(ya,yv,false);
		}
	}
	
	private void rotYLeft(boolean dir) {
		Stone s=findStone(za,-zv,ya,-yv);
		move(ya,-yv,true);
		if((s.coordinate()[xa]==xv)!=dir) {
			move(ya,-yv,false);
			move(ya,-yv,false);
		}
	}
	
	private void rotZ(boolean dir) {
		Stone s=findStone(za,-zv,xa,xv);
		move(za,-zv,true);
		if((s.coordinate()[ya]==yv)!=dir) {
			move(za,-zv,false);
			move(za,-zv,false);
		}
	}

	private void rotX(boolean dir) {
		Stone s=findStone(xa,xv,ya,yv);
		move(xa,xv,true);
		if((s.coordinate()[za]==-zv)!=dir) {
			move(xa,xv,false);
			move(xa,xv,false);
		}
	}

	
	protected void f1() {
		rotY(true);
	}
	
	protected void f2() {
		rotZ(true);
	}
	
	protected void f3() {
		rotX(true);
	}
	
	protected void f4() {
		rotY(false);
	}
	
	protected void f5() {
		rotZ(false);
	}
	
	protected void f6() {
		rotX(false);
	}

	protected void f7() {
		rotYLeft(true);
	}

	protected void f8() {
		rotYLeft(false);
	}

}
