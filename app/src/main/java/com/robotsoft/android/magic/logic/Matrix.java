package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;
public class Matrix {

	public static int getOrder(Point p) {
		return getOrder(p.getAxis(),p.getValue());
	}
	
	public static int getOrder(int a,int v) {
		return v<0?2-a:3+a;
	}

	public static Point getPoint(int order) {
		int v=order<3?-1:1;
		return new Point(v==-1?2-order:order-3, v);
	}
	
	public static int getAction(Operator o) {
		return getAction(o.getRot(),o.getPos(),o.getDir());
	}
	
	public static int getAction(int rot,int pos, boolean dir) {
		int p=rot*3+pos+1;
	    if(dir)p+=9;
		return p;
	}

	public static Operator getOperator(int action) {
		boolean dir=false;
		if(action>=9) {
			action-=9;
			dir=true;
		}
		int rot=action/3;
		int pos=action%3-1;
		
		return new Operator(rot, pos, dir);
	}
	
	public static int transform(int p, int[][] mat) {
		Operator o=getOperator(p);
		int rot=o.getRot();
		int pos=o.getPos();
		boolean dir=o.getDir();
		int[][] m=product(mat, rotate(rot, dir));
		for(int i=0; i<3; i++) {
			if(mat[i][rot]!=0) {
				rot=i;
				if(mat[i][rot]==-1) {
					pos=-pos;
				}
				break;
			}
		}
		int[][] res=rotate(rot, dir);
		int v=(rot==2)?0:rot+1;
	    int w=(rot==0)?2:rot-1;
	    if(m[v][w]!=res[v][w]) {
	    	dir=!dir;
	    }
	    return getAction(rot, pos, dir);
	}
	
	
	public static int[][] product(int[][] a,int[][] b) {
		int[][] c=new int[3][3];
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				c[i][j]=a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
			}
		}
		return c;
	}
	
	public static int[][] rotate(int u, boolean dir) {
		int[][] m=new int[3][3];
	    m[u][u]=1;
	    int v=(u==2)?0:u+1;
	    int w=(u==0)?2:u-1;
	    m[v][w]=dir?-1:1;
	    m[w][v]=-m[v][w];
	    return m;
	}
	
	
	public static int[][] reverse(int[][] a) {
		int[][] res = new int[3][3];
		int d=a[0][0]*(a[1][1] * a[2][2] - a[1][2] * a[2][1])
		+a[0][1]*(a[1][2] * a[2][0] - a[1][0] * a[2][2])
		+a[0][2]*(a[1][0] * a[2][1] - a[1][1] * a[2][0]);
		for (int i = 0; i < 3; i++) {
			int i1 = i == 2 ? 0 : i + 1;
			int i2 = i1 == 2 ? 0 : i1 + 1;
			for (int j = 0; j < 3; j++) {
				int j1 = j == 2 ? 0 : j + 1;
				int j2 = j1 == 2 ? 0 : j1 + 1;
				res[i][j]=d*(a[j1][i1] * a[j2][i2] - a[j2][i1] * a[j1][i2]);
			}
		}
		return res;
	}

	public static int[][][] allMatrix() {
		int[][][] res = new int[32][][];
		int l = 1;
		int[][] f = new int[3][3];
		f[0][0] = 1;
		f[1][1] = 1;
		f[2][2] = 1;
		res[0]=f;
		for (int i = -1; i <= 1; i += 2) {
			for (int j = -1; j <= 1; j += 2) {
				for (int k = -1; k <= 1; k += 2) {
					if(i==1&&j==1&&k==1) {
						continue;
					}
					f = new int[3][3];
					f[0][0] = i;
					f[1][1] = j;
					f[2][2] = k;
					res[l++] = f;
				}
			}
		}
		for (int u = 0; u < 3; u++) {
			int v=(u==2)?0:u+1;
			int w=(v==2)?0:v+1;
			for (int i = -1; i <= 1; i += 2) {
				for (int j = -1; j <= 1; j += 2) {
					for (int k = -1; k <= 1; k += 2) {
						f = new int[3][3];
						f[u][u] = i;
						f[v][w] = j;
						f[w][v] = k;
						res[l++] = f;
					}
				}
			}
		}
		return res;
	}
	
	public static void print(int[][] m) {
		for(int j=0; j<3; j++) {
			for(int k=0; k<3; k++) {
				System.out.print("  "+m[j][k]);
			}
			System.out.println();
		}
	
	}
	
	
	public static void main(String[] argv) {
		int[][][] f=allMatrix();
		for(int i=0; i<32; i++) {
			int[][] a=f[i];
			int[][] b=reverse(a);
			//print(a);
			//print(b);
			int[][] c=new int[3][3];
				
			for(int j=0; j<3; j++) {
				for(int k=0; k<3; k++) {
					int d=0;
					for(int t=0; t<3; t++) {
						d+=a[j][t]*b[t][k];
					}
					c[j][k]=d;
				}
			}
			print(c);
		}
	}
	
	public static Stone findCenter(Stone[] stones, int index) {
		for(int i=0; i<stones.length; i++) {
			if(stones[i].getType()==1&&stones[i].getIndexs()[0]==index) {
				return stones[i];
			}
		}
		return null;
	}
	
	
	public static Stone findEdge(Stone[] stones, int prime, int index) {
		for (int i = 0; i < stones.length; i++) {
			if (stones[i].getType() == 2) {
				int a = stones[i].getIndexs()[0];
				int b = stones[i].getIndexs()[1];
				if ((a == prime && b == index) || (a == index && b == prime)) {
					stones[i].setHint(a == prime ? 0 : 1);
					return stones[i];
				}
			}
		}
		return null;
	}

	
	public static Stone findCorner(Stone[] stones, int prime, int a, int b) {
		for(int i=0; i<stones.length; i++) {
			if(stones[i].getType()==3) {
				int[] d=stones[i].getIndexs();
				int u=d[0];
				int v=d[1];
				int w=d[2];
				if(u==prime||v==prime||w==prime) {
					if(u==a||v==a||w==a) {
						if(u==b||v==b||w==b) {
							stones[i].setHint(u==prime?0:v==prime?1:2);
							return stones[i];
						}	
					}	
				}
			}
		}
		return null;
	}

	public static boolean isSame(int[][] ss, int[][] tt) {
		for(int i=0; i<3; i++)for(int j=0; j<3; j++) {
			if(ss[i][j]!=tt[i][j]) return false;
		}
		return true;
	}


}
