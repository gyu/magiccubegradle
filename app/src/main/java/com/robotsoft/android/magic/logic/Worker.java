package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;



public abstract class Worker {
	protected MoveSupport host;
	
	public abstract boolean process(int state);
	
	public Worker(MoveSupport host) {
		this.host = host;
	}
	
	public boolean process() {
		return process(-1);
	}
	
	protected void move(int rot,int pos,boolean dir) {
		host.move(rot, pos, dir);
	}
	
	protected boolean centerSucceed(int index) {
		Stone stone=Matrix.findCenter(host.getStones(), index);
		int[] a=stone.getData();
		int[] b=stone.coordinate();
		return (a[0]==b[0]&&a[1]==b[1]&&a[2]==b[2]);
	}

}
