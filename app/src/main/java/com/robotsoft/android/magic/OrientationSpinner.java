package com.robotsoft.android.magic;

import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.Log;

public class OrientationSpinner extends LightSpinner {
	public OrientationSpinner(Context ctx, int x, int y, int w, int h,int b) {
		super(ctx, x, y, w, h, b, new Object[]{Boolean.TRUE,Boolean.FALSE});
	}
	
	public void paintContent(Canvas g) {
		Paint p=new Paint();
		p.setStyle(Style.STROKE); 
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_text:R.color.menu_text));
		p.setAntiAlias(true);
		if(w>30) p.setStrokeWidth(1f*w/30);
		RectF rct=new RectF(x + w/8, y + h / 4, x+w*7/8, y+h*3/4); 
		g.drawArc(rct, -135, 270, false, p);
		int d = index==0 ? y + h /4+1 : y + h *3/4-2;
		int delta=Math.max(3, h/10);
		for(int i=-delta; i<=delta; i++) {
		    g.drawLine(x+2+w/8, d+index, x+w/8+w*11/50, d+i+index, p);	
		}
	}
}
