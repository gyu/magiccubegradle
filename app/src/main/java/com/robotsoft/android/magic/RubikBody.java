package com.robotsoft.android.magic;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import com.robotsoft.android.base.Cursor;


public class RubikBody implements Focusable {
	 
	                     //colors =  {0xffffff00,0xfff80000,0xffffffff,0xff0000f8,0xffff00ff,0xff00f800,0xff142800 };
	
	private static int[][] colors =  {{0xffffff00,0xffff7f00},{0xffff0000,0xff7f0000},{0xffffffff,0xff7f7f7f},{0xff0000ff,0xff00007f},
		{0xffff00ff,0xff7f007f},{0xff00ff00,0xff007f00},{0xff142800} };
	private Renderer model;
	private View parent;
	private int offx;
	private int offy;
	private int dim;
	private int width;
	private int height;
	private int[] pnts;
	private boolean selected;
	private boolean mouseDown;
	private PositionHolder holder;
	private float span;
	private int rad;
	private int ox;
	private int oy;
	private Paint txtPaint;
	private int axisSpan;
		

	public RubikBody(View parent, Animator animator,  GameData game) {
		this.parent = parent;
		model = new Renderer(parent, animator, game);
	}

	public void setDimension(int width, int height) {
			this.width = width;
			this.height = height;
			axisSpan=Math.min(width, height)/6;
			ox = width- axisSpan;
			oy = height - axisSpan;
			txtPaint=new Paint();
			txtPaint.setColor(parent.getResources().getColor(
					R.color.label_text));
			txtPaint.setAntiAlias(true);
			txtPaint.setTextSize(16);
			txtPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL));
			txtPaint.setAntiAlias(true);
			rad=(Math.min(width, height)+40)/100;
			dim = Math.min(width, height)/12;
			if (width > height) {
				offy = height / 2;
				offx = height * 5 / 11;
			} else {
				offx = width / 2;
				offy = height * 6 / 13;
				if(width<350){
					offx-=20;
				}
			}
			span=Math.max(2f, dim*0.1f);
	}

	public Renderer getModel() {
		return model;
	}

	public boolean gainFocus(boolean dir, int index) {
		boolean b = !selected;
		if (holder == null) {
			holder = new PositionHolder(model.getMiddlePoints(), offx, offy,
					dim, width, height);
		}
		pnts = holder.getFirst(index == 5);
		selected = true;
		return b;
	}

	public int cursorX() {
		return holder.getColumn();
	}

	public int cursorY() {
		return holder.getRow();
	}

	public boolean hasFocus() {
		return selected;
	}

	public boolean isDown() {
		return mouseDown;
	}

	public boolean lostFocus(int dx, int dy) {
		if (!selected)
			return false;
		if (mouseDown) {
			if(pnts!=null)
			model.rotateCube(1.0 * (pnts[0] - offx) / dim, 1.0 * (pnts[1] - offy) / dim, dx, dy);
			return false;
		}
		if (Math.abs(dx) > Math.abs(dy)) {
			dy = 0;
			dx = (dx < 0) ? -1 : 1;
		} else {
			dx = 0;
			dy = (dy < 0) ? -1 : 1;
		}
		pnts = holder.moved(dx, dy);
		if (pnts == null) {
			return false;
		}
		if (pnts.length < 2) {
			selected = false;
			return true;
		}
		return false;
	}

	public int mouseUp() {
		if (selected)
			mouseDown = !mouseDown;
		return -1;
	}
	
	

	public void paint(Canvas canvas) {
		paintAxis(canvas);
		
		
		List<Path> list = new ArrayList<Path>();
		List<Square> squares = model.getSquares();
		//int[] data = new int[width * height];
		Paint linePaint=new Paint();
		linePaint.setAntiAlias(true);
		linePaint.setColor(0xff000000);
		linePaint.setStyle(Style.STROKE);
		linePaint.setStrokeWidth(span);
		for (int i = 0; i < squares.size(); i++) {
			Square s = squares.get(i);
			if (s.getNumber() == -1 || s.getXy() == null) {
				for (Path path:list) {
					canvas.drawPath(path, linePaint);
				}
				list.clear();
			} else {
				Path p=new Path();
				double[][] xy = s.getXy();
				p.moveTo((float)(xy[0][0] * dim + offx),(float)( xy[0][1] * dim + offy));
				Paint shapePaint=new Paint();
				//shapePaint.setAntiAlias(true);
				shapePaint.setColor(colors[s.getNumber()][0]);
				shapePaint.setStyle(Style.FILL);
				for (int j = 1; j < 4; j++) {
					p.lineTo((float)(xy[j][0] * dim + offx),(float)( xy[j][1] * dim + offy));
				}
				canvas.drawPath(p, shapePaint);
				list.add(p);
			}
		}
		for (Path path:list) {
			canvas.drawPath(path, linePaint);
		}
		//canvas.drawBitmap(data, 0, width, 0, 0, width, height, true, null);
	}

	
	private void paintAxis(Canvas g) {
		try {
			double[][] s = model.axisData();
			for (int i = 0; i < 3; i++) {
				float x = (float) s[i][0] * axisSpan + ox;
				float y = (float) s[i][1] * axisSpan + oy;
				g.drawLine(ox, oy, x, y, txtPaint);
				if(x>width-10) {
					y=oy+(width-10-ox)*(y-oy)/(x-ox);
					x=width-10;
				}
				if(y>height-10) {
					x=ox+(height-10-oy)*(x-ox)/(y-oy);
					y=height-10;
				}
				String q = (i == 0) ? "x" : (i == 1) ? "y" : "z";
				g.drawText(q, x + rad*2/3, y - rad*4/3, txtPaint);
			}
			g.drawCircle(ox, oy, rad, txtPaint);
		} catch (Exception ex) {
			Log.e("Rubik", "Erron on draw", ex);
		}
	}

	public void rotate() {
		holder = null;
	}

	public void rotate(int dx, int dy) {
		if(pnts!=null) {
		if (Math.abs(dx) > Math.abs(dy)) {
			dy = 0;
		} else {
			dx = 0;
		}
		model.rotateCube(1.0 * (pnts[0] - offx) / dim, 1.0 * (pnts[1] - offy) / dim, dx, dy);
		}
	}

	public boolean in(int xx, int yy, int range) {
		if(range==0) {
			if (holder == null) {
				holder = new PositionHolder(model.getMiddlePoints(), offx, offy,
						dim, width, height);
			}
			pnts = holder.checkPosition(xx, yy);
			if (pnts == null) {
				return false;
			}
			return true;
		}
		if(pnts!=null) {
			if(Math.abs(pnts[0]-xx)<=range||Math.abs(pnts[1]-yy)<=range) {
				return (pnts[0]-xx)*(pnts[0]-xx)+(pnts[1]-yy)*(pnts[1]-yy)<=range*range;
			}
		}
		return false;
	}

	public void setSelected(boolean b) {
		selected = b;
		if(!b) pnts=null;
		if (b)
			mouseDown = b;
	}

	public boolean lostFocus() {
		selected = false;
		pnts=null;
		return false;
	}


}
