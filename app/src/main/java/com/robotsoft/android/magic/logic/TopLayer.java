package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class TopLayer extends Above {
	
	public TopLayer(MoveSupport host, int root) {
		super(host, root);
	}

	public boolean process(int state) {
		if(state==7) {
			return true;
		}
		if(state<3) {
			MiddleLayer layer = new MiddleLayer(host, root);
			boolean b=layer.process(state);
			if(!b||state!=-1) return b;
		}	
		face = Cubes[root][0][0];
		right = Cubes[root][1][0];
		fix();
		if (state == 3 || state == -1) {
			boolean b = makeCross();
			if (!b) {
				System.out.println("Make Cross failed.");
				return false;
			}
			if (state != -1) {
				return true;
			}
		}
		if(state==4||state==-1) {
			boolean b=fillTop();
			if (!b) {
				System.out.println("Cannot resolve corner");
				return false;
			}
			if(state!=-1) {
				return true;
			}
		}
		if(state==5||state==-1) {
		    boolean b=resolveConers(0);
		    if (!b) {
			   System.out.println("Cannot resolve corner");
				
		    }
		    if(state!=-1) {
				return true;
			}
		}
	    return finalPut();
	}

	private boolean faceEdgeOkay(Stone s) {
		int[] in = s.getIndexs();
		int i = in[0] == 5 - root ? 0 : 1;
		int[] f = s.getFaces()[i];
		return (f[za] == -3 * zv);
	}

	private boolean crossOkay() {
		for (int i = 0; i < 4; i++) {
			int a = (i < 2) ? xa : ya;
			int v = (i % 2 == 0) ? -1 : 1;
			Stone s = findStone(za, -zv, a, v);
			if (!faceEdgeOkay(s)) {
				return false;
			}
		}
		return true;
	}

	private boolean makeCross() {
		if (crossOkay()) {
			return true;
		}
		int[][][] data = host.fetchData();
		for (int i = 0; i < 4; i++) {
			host.mark();
			for (int j = 0; j < i; j++) {
				move(za, -zv, true);
			}
			f123564();
			if (crossOkay()) {
				host.unmark();
				return true;
			}
			host.resetData(data);
			host.undo();
		}
		int a=-1;
		int aa=-1;
		host.mark();
		LOOP:for (int i = 0; i < 4; i++) {
			for (int j = 0; j < i; j++) {
				move(za, -zv, true);
			}
			f123564();
			int[][][] data2 = host.fetchData();
			for (int ii = 0; ii < 4; ii++) {
				for (int jj = 0; jj < ii; jj++) {
					move(za, -zv, true);
				}
				f123564();
				if (crossOkay()) {
					a=i;
					aa=ii;
					host.resetData(data);
					break LOOP;
				}
				else {
					host.resetData(data2);
				}
			}
			host.resetData(data);
		}
		host.undo();
		if(a!=-1) {
			for (int j = 0; j < a; j++) {
				move(za, -zv, true);
			}
			f123564();
			for (int jj = 0; jj < aa; jj++) {
				move(za, -zv, true);
			}
			f123564();
			return true;
		}
		int aaa=-1;
		host.mark();
		LOOP2:for (int i = 0; i < 4; i++) {
			for (int j = 0; j < i; j++) {
				move(za, -zv, true);
			}
			f123564();
			int[][][] data2 = host.fetchData();
			for (int ii = 0; ii < 4; ii++) {
				for (int jj = 0; jj < ii; jj++) {
					move(za, -zv, true);
				}
				f123564();
				int[][][] data3 = host.fetchData();
				for (int iii = 0; iii < 4; iii++) {
					for (int jjj = 0; jjj < iii; jjj++) {
						move(za, -zv, true);
					}
					f123564();
					if (crossOkay()) {
						a=i;
						aa=ii;
						aaa=iii;
						host.resetData(data);
						break LOOP2;
					}
					else 
						host.resetData(data3);
				}
				host.resetData(data2);
			}
			host.resetData(data);
		}
		host.undo();
		if(a!=-1) {
			for (int j = 0; j < a; j++) {
				move(za, -zv, true);
			}
			f123564();
			for (int j = 0; j < aa; j++) {
				move(za, -zv, true);
			}
			f123564();
			for (int j = 0; j < aaa; j++) {
				move(za, -zv, true);
			}
			f123564();
			return true;
		}
		System.out.println("Cannot build cross configuration");
		return false;
	}

	private int checkType() {
		Stone[] ss = findRestCorners();
		Stone s = null;
		int k = -1;
		for (int i = 0; i < 4; i++) {
			int[] in = ss[i].getIndexs();
			int j = in[0] == 5 - root ? 0 : in[1] == 5 - root ? 1 : 2;
			int[] f = ss[i].getFaces()[j];
			if (f[za] == -3 * zv) {
				if (s != null) {
					return -1;
				} else {
					s = ss[i];
					k = i;
				}
			}
		}
		if (s == null)
			return -1;
		int[] w1 = s.coordinate();
		boolean t = true;
		int kk = k;
		for (int ii = 0; ii < 3; ii++) {
			kk = (kk == 0) ? 3 : kk - 1;
			Stone z = ss[kk];
			int[] w2 = w1;
			w1 = z.coordinate();
			int j = w1[xa] == w2[xa] ? xa : ya;
			int[] in = z.getIndexs();
			int jj = in[0] == 5 - root ? 0 : in[1] == 5 - root ? 1 : 2;
			int[] f = z.getFaces()[jj];
			if (f[j] != w1[j] * 3) {
				t = false;
				break;
			}
		}
		if (t)
			return 0;

		kk = k;
		w1 = s.coordinate();
		for (int ii = 0; ii < 3; ii++) {
			kk = (kk == 3) ? 0 : kk + 1;
			Stone z = ss[kk];
			int[] w2 = w1;
			w1 = z.coordinate();
			int j = w1[xa] == w2[xa] ? xa : ya;
			int[] in = z.getIndexs();
			int jj = in[0] == 5 - root ? 0 : in[1] == 5 - root ? 1 : 2;
			int[] f = z.getFaces()[jj];
			if (f[j] != w1[j] * 3) {
				return -1;
			}
		}
		return 1;
	}

	private boolean cornerOkay() {
		Stone[] stones = findRestCorners();
		for (int i = 0; i < 4; i++) {
			Stone t = stones[i];
			int[] types = t.getIndexs();
			int in = (types[0] == 5 - root) ? 0 : (types[1] == 5 - root) ? 1
					: 2;
			int[] f = t.getFaces()[in];
			if (f[za] != -3 * zv)
				return false;
		}
		return true;
	}

	private boolean cornerExactOkay() {
		adjustCorners();
		Stone[] stones = findRestCorners();
		for (int i = 0; i < 4; i++) {
			Stone t = stones[i];
			int[] types = t.getIndexs();
			int in = (types[0] == 5 - root) ? 0 : (types[1] == 5 - root) ? 1
					: 2;
			int[] f = t.getFaces()[in];
			if (f[za] != -3 * zv)
				return false;
			int[] w1 = t.coordinate();
			for (int j = 0; j < 3; j++) {
				Stone p = findCenter(types[j]);
				int[] w2 = p.coordinate();
				for (int k = 0; k < 3; k++) {
					if (w2[k] != 0 && w2[k] != w1[k]) {
						return false;
					}
				}
			}
		}
		return true;
	}

	private boolean fillTop() {
		int type = checkType();
		if (type == -1) {
			int[][][] data = host.fetchData();
			for (int i = 0; i < 4; i++) {
				host.mark();
				for (int j = 0; j < i; j++) {
					move(za, -zv, true);
				}
				f124215545();
				type = checkType();
				if (type == -1) {
					host.resetData(data);
					host.undo();
				} else {
					host.unmark();
					break;
				}
			}
		}
		if (type == -1) {
			System.out.println("Cannot fill the top ");
			return false;
		}
		int[][][] data = host.fetchData();
		for (int i = 0; i < 4; i++) {
			host.mark();
			for (int j = 0; j < i; j++) {
				move(za, -zv, true);
			}
			if (type == 0) {
				f451542212();
			} else if (type == 1) {
				f124215545();
			}
			if (cornerOkay()) {
				host.unmark();
				return true;
			}
			host.undo();
			host.resetData(data);
		}
		return true;
	}

	int getFaceType(Stone s, int a, int v) {
		int[][] fs = s.getFaces();
		for (int i = 0; i < fs.length; i++) {
			if (fs[i][a] == 3 * v) {
				return s.getIndexs()[i];
			}
		}
		return -1;
	}

	private Stone findCenter(int index) {
		Stone[] stones = host.getStones();
		for (int i = 0; i < stones.length; i++) {
			if (stones[i].getType() == 1 && stones[i].getIndexs()[0] == index) {
				return stones[i];
			}
		}
		return null;
	}

	private boolean cornerOkay(Stone s) {
		int[] types = s.getIndexs();
		int[] w = s.coordinate();
		for (int i = 0; i < 3; i++) {
			Stone t = findCenter(types[i]);
			int[] u = t.coordinate();
			for (int j = 0; j < 3; j++) {
				if (u[j] != 0 && u[j] != w[j]) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean edgeOkay(Stone s) {
		int[] types = s.getIndexs();
		int i = (types[0] == 5 - root) ? types[1] : types[0];
		Stone t = findCenter(i);
		int[] u = t.coordinate();
		int[] w = s.coordinate();
		for (int j = 0; j < 3; j++) {
			if (u[j] != 0 && u[j] != w[j]) {
				return false;
			}
		}
		return true;
	}

	private void adjustCorners() {
		Stone ss = findRestCorners()[0];
		for (int i = 0; i < 4; i++) {
			if (cornerOkay(ss)) {
				break;
			} else {
				move(za, -zv, true);
			}
		}
	}

	
	private boolean finalCheck() {
		Stone[] stones=host.getStones();
		for(int i=0; i<26; i++) {
			int[] u=stones[i].getData();
			int[] w=stones[i].coordinate();
			if(u[0]!=w[0]||u[1]!=w[1]||u[2]!=w[2]) {
				System.out.println(" Stone "+i+" does not match");
				
				return false;
			}
			
		}
		return true;
		
	}
	
	private boolean finalPut() {
		for (int ii = 0; ii < 4; ii++) {
			Stone[] s = findTopEdges();
			Stone t = null;
			int k = 0;
			for (int i = 0; i < 4; i++) {
				if (edgeOkay(s[i])) {
					t = s[i];
					k++;
				}
			}
			if (k == 4) {
				return true;
			} else if (k == 0) {
				f124215545();
				f451542212();
			} else {
				int[] types = t.getIndexs();
				face = types[0] == 5 - root ? types[1] : types[0];
				for (int i = 0; i < 4; i++) {
					if (Cubes[root][i][0] == face) {
						right = Cubes[root][i][1];
					}
				}
				fix();
				f124215545();
				f451542212();
			}
		}
		return true;
	}

	private int findLeftResolveConers() {
		Stone[] st = findRestCorners();
		int[] w1 = st[3].coordinate();
		Stone s = st[3];
		for (int i = 0; i < 4; i++) {
			Stone t = s;
			s = st[i];
			int[] w2 = w1;
			w1 = st[i].coordinate();
			int j = (w1[xa] == w2[xa]) ? xa : ya;
			int t1 = getFaceType(t, j, w1[j]);
			if (t1 != -1) {
				int t2 = getFaceType(s, j, w1[j]);
				if (t1 == t2) {
					return w1[j] > 0 ? 3 + j : 2 - j;
				}
			}
		}
		return -1;
	}

	private boolean resolveConers(int count) {
		if (cornerExactOkay()) return true;
		if(count==3) {
			System.out.println("Conner cannot be resolved.");
			return false;
		}
		int left = findLeftResolveConers();
		if (left == -1) {
			int[][][] data = host.fetchData();
			for (int i = 0; i < 4; i++) {
				host.mark();
				for (int j = 0; j < i; j++) {
					move(za, -zv, true);
				}
				f616773467766();
				left = findLeftResolveConers();
				if (left >= 0) {
					host.unmark();
					break;
				} else {
					host.undo();
					host.resetData(data);
				}
			}
		}
		if (left == -1) {
			System.out.println("Cannot resolve corner!");
			return false;
		}
		right = 5 - left;
		for (int i = 0; i < 4; i++) {
			if (Cubes[root][i][1] == right) {
				face = Cubes[root][i][0];
				break;
			}
		}
		fix();
		f616773467766();
		return resolveConers(count+1);
	}

	private void f451542212() {
		f4();
		f5();
		f1();
		f5();
		f4();
		f2();
		f2();
		f1();
		f2();
	}

	private void f124215545() {
		f1();
		f2();
		f4();
		f2();
		f1();
		f5();
		f5();
		f4();
		f5();
	}

	private void f616773467766() {
		f6();
		f1();
		f6();
		f7();
		f7();
		f3();
		f4();
		f6();
		f7();
		f7();
		f6();
		f6();
	}

	private void f123564() {
		f1();
		f2();
		f3();
		f5();
		f6();
		f4();
	}

	private Stone[] findRestCorners() {
		Stone[] res = new Stone[4];
		Stone[] stones = host.getStones();
		for (int i = 0; i < stones.length; i++) {
			if (stones[i].getType() == 3) {
				int[] w = stones[i].coordinate();
				if (w[za] == -zv) {
					int j = (w[xa] == xv && w[ya] == yv) ? 0
							: (w[xa] == xv && w[ya] == -yv) ? 1
									: (w[xa] == -xv && w[ya] == -yv) ? 2 : 3;
					res[j] = stones[i];
				}
			}
		}
		return res;
	}

	private Stone[] findTopEdges() {
		Stone[] res = new Stone[4];
		Stone[] stones = host.getStones();
		for (int i = 0; i < stones.length; i++) {
			if (stones[i].getType() == 2) {
				int[] in = stones[i].getIndexs();
				if (in[0] == 5 - root || in[1] == 5 - root) {
					int[] w = stones[i].coordinate();
					int j = (w[xa] == xv) ? 0 : (w[ya] == yv) ? 1
							: (w[xa] == -xv) ? 2 : 3;
					res[j] = stones[i];
				}
			}
		}
		return res;
	}

}
