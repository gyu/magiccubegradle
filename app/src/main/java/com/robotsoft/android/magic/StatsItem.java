package com.robotsoft.android.magic;

import com.robotsoft.android.base.AbstractStatsItem;

public class StatsItem extends AbstractStatsItem {
	private byte[] shuffels;
	private byte[] operations;
	
	public StatsItem(int timePlayed, byte[] shuffels, byte[] operations) {
		this(-1,timePlayed, shuffels,operations);
	}
	
	public StatsItem(int id, int timePlayed, byte[] shuffels, byte[] operations) {
		super(id, timePlayed);
		this.shuffels = shuffels;
		this.operations = operations;
	}

	public byte[] getShuffels() {
		return shuffels;
	}
	public void setShuffels(byte[] shuffels) {
		this.shuffels = shuffels;
	}
	public byte[] getOperations() {
		return operations;
	}
	public void setOperations(byte[] operations) {
		this.operations = operations;
	}
}
