package com.robotsoft.android.magic;

import com.robotsoft.android.base.Cursor;
import com.robotsoft.android.magic.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

public class LightSpinner extends LightButton {
	private Object[] values;
	protected int index;
	protected int bar;
	protected static int space=4;
	
	public LightSpinner(Context context, int x, int y, int w, int h, int bar, Object[] values) {
		super(context, x, y, w-bar-space, h, -1, values[0].toString());
		this.values = values;
		index = 0;
		this.bar = bar;
	}

	public void paint(Canvas g) {
		name=values[index].toString();
		super.paint(g);
	}
	
		
	public Object getSelected() {
		return values[index];
	}
	
	public int getIndex() {
		return index;
	}

	public void paintBox(Canvas g) {
		super.paintBox(g);
		paintBox(g,x+w+space,y,bar,h);
		Paint p=new Paint();
		p.setColor(context.getResources().getColor(selected?R.color.menu_sel_text:R.color.menu_text));
		for(int i=-5; i<=5; i++) {
			g.drawLine(x+w+space+bar/2+i, y+h/2-4, x+w+space+bar/2, y+h/2+5,p);
		}
	}

	public int mouseUp() {
		if(selected) {
		   	index++;
		   	if(index==values.length)index=0;
		   	return index;
		}
		return -1;
	}
	
	public void drawCursor(Canvas g, Cursor c) {
		if(selected) {
			c.paint(context, g, Cursor.DefState, x+w+space+bar/2, y+h/2);
		}
	}
	
	public boolean in(int xx,int yy, int d) {
		int z=x+w+space;
		return xx>=x-d&&xx<z+bar+d&&yy>=y-d-20&&yy<y+h+d+20;	
	}

}
