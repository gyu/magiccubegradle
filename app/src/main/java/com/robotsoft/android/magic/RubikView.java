package com.robotsoft.android.magic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.robotsoft.android.base.AbstractDatabase;
import com.robotsoft.android.base.AbstractView;
import com.robotsoft.android.base.Clock;
import com.robotsoft.android.base.Cursor;
import com.robotsoft.android.base.UIPreference;
import com.robotsoft.android.magic.logic.HintProvider;
import com.robotsoft.android.magic.logic.StateCalculator;

public class RubikView extends AbstractView {
	private RubikBody body;
	private Focusable[] componnents;
	private int selectedIndex;
	private Animator animator;
	private MessagePane messagePane;

	public RubikView(Context context) {
		super(context);
		componnents = new Focusable[6];
		animator = new Animator(true);
	}

	@Override
	public void showHint() {
		if(animation) return;
		animation= true;
		Runnable runnable = new Runnable() {
			public void run() {
				HintProvider hp = new HintProvider(body.getModel(),
						((GameData)getGameData()).getOperations());
				final int[] hints = hp.getHints();
				messagePane=new MessagePane(getContext(), hp.getState(), width, height);
				body.getModel().rotateMatrix(hp.getRotAxis());
				Runnable runable = new Runnable() {
					public void run() {
						body.getModel().startRotation(hints, false);
					}
				};
				handler.post(runable);
			}
		};
		new Thread(runnable).start();
	}

	@Override
	public Rect getBorderBounds() {
		return null;
	}

	@Override
	public void undo() {
		body.getModel().goBack();
	}

	@Override
	protected void doInit() {
		body = new RubikBody(this, animator, (GameData) database.getGameData());
		componnents[0] = body;
		selectedIndex = -1;
		body.setDimension(width, height);
		int sz=Math.min(width, height);
		int MenuWidth = sz/12;
		int MenuHeight = sz/15;
		int y0 = sz/24;
		int MenuLen = sz/5;
		int SpinnerLen = sz/6;
		int BoxLen = sz/18;
		int Offset = sz/80;
		int x0=width - MenuLen * 3-MenuWidth-Offset;
		LightSpinner s1 = new LightSpinner(getContext(), x0, y0, SpinnerLen, MenuHeight, BoxLen,
				new Object[] { "x", "y", "z" });
		LightSpinner s2 = new LightSpinner(
				getContext(),
				x0+ MenuLen, y0,
				SpinnerLen,
				MenuHeight,
				BoxLen,
				new Object[] { new Integer(-1), new Integer(0), new Integer(1) });
		LightSpinner s3 = new OrientationSpinner(getContext(), x0+MenuLen* 2, y0, SpinnerLen, MenuHeight, BoxLen);
		componnents[1] = s1;
		componnents[2] = s2;
		componnents[3] = s3;
		componnents[4] = new RunButton(getContext(), x0+ MenuLen*3,
				y0, MenuWidth, MenuHeight, new LightSpinner[] { s1, s2,
						s3 }, body.getModel());
		componnents[5] = new BackButton(getContext(), Offset, y0,
				MenuWidth, MenuHeight, body.getModel());
	}


	@Override
	protected int stateAfterMove() {
		return 0;
	}

	@Override
	protected AbstractDatabase create(Context context) {
		return new Database(context);
	}

	@Override
	protected void doDraw(Canvas canvas) {
		try {
			for (int i = 0, n = componnents.length; i < n; i++) {
				componnents[i].paint(canvas);
			}
			
		} catch (Throwable ex) {
			Log.e("Rubik", "Drawing problem", ex);
		}
	}
	
	protected void drawMessage(Canvas canvas) {
		if(messagePane!=null) {
			messagePane.paint(canvas);
		}
	}

	@Override
	protected void clear() {
	}

	@Override
	protected boolean pointerPressed(int x, int y) {
		int k=-1;
		for (int i = 0; i < componnents.length; i++) {
			if (componnents[i].in(x, y, 0)) {
				k=i;
				break;
			}
		}
		int d=UIPreference.getGestureThreshold(getResources());
		if(k==-1) {
			for (int i = 1; i < componnents.length; i++) {
				if (componnents[i].in(x, y, d)) {
					k=i;
					break;
				}
			}
		}
		if(k==-1&&selectedIndex!=-1&&componnents[selectedIndex].in(x, y, 2*d)) {
			k=selectedIndex;
		}
		if (k!=selectedIndex) {
			if(selectedIndex!=-1) componnents[selectedIndex].setSelected(false);
			selectedIndex=k;
			if(selectedIndex!=-1)componnents[selectedIndex].setSelected(true);
		}
		return true;
	}

	@Override
	protected void pointerReleased(int x, int y, boolean flag) {
		if (flag) {
			checkAction();
		}
	}

	@Override
	protected boolean pointerDragged(int dx, int dy) {
		if(selectedIndex==-1) {
			body.getModel().rotateMatrix(dy, dx);
			body.rotate();
			return false;
		}
		else if(selectedIndex==0){
			body.rotate(dx, dy);
			return true;
		}
		return true;
	}

	@Override
	protected void stepDone() {
	}

	public void startRotation() {
		animation = true;
		Fly fly = new Fly();
		fly.setRepeatCount(animator.getTotalSteps());
		fly.setDuration(60);
		startAnimation(fly);
	}

	public void stopAnimation(boolean finished) {
		clearAnimation();
		messagePane = null;
		animation = false;
		if (finished) {
			oneMove(Complete);
		}
		else {
			oneMove(Step);
		}
	}

	private void checkAction() {
		for (int i = 0, n = componnents.length; i < n; i++) {
			componnents[i].mouseUp();
		}
		invalidate();
	}

	private class Fly extends Animation {
		
		@Override
		protected void applyTransformation(float interpolatedTime,
				Transformation t) {
			int state = animator.getState();
			if (state <= 0) {
				if (state == 0) animator.prepareDraw();
				else animator.setIdle();
				return;
			}
			animator.setIdle();
			if (animator.doStore()) {
				((Database) database).newMove(animator.getMove(), clock.getTime());
			}
			StateCalculator sc = new StateCalculator(body.getModel().getStones());
			boolean allDone = sc.getState() == StateCalculator.AllDone;
			if (state == 2 ||allDone) {
				stopAnimation(allDone);
			}
			else {
				oneMove(Step);
			}
		}
	}
	
	public void showMessage(String msg) {
		if (msg == null) {
			if (messagePane != null) {
				messagePane = null;
			}
		} else {
			messagePane = new MessagePane(getContext(), msg, width, height);
		}
		invalidate();
	}

	@Override
	protected void moveAround() {
	}

	@Override
	protected boolean navigate(int dx, int dy) {
		return false;
	}

	@Override
	protected boolean navigate() {
		return false;
	}
}
