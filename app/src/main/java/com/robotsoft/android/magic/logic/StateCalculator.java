package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class StateCalculator {
	public final static int FirstCross=0;
	public final static int FirstLayer=1;
	public final static int MiddleLayer=2;
	public final static int TopCross=3;
	public final static int TopFilled=4;
	public final static int CornerDone=5;
	public final static int AllDone=6;
	private Stone[] stones;
	private int[][][] faces;
	private int[][][] data;
	private int[][] prime;
	private int norm=0;
	
	public StateCalculator(Stone[] stones) {
		this.stones = stones;
		faces = new int[6][3][3];
		for (int i = 0; i < 26; i++) {
			int[] o = stones[i].coordinate();
			int[] d = stones[i].getIndexs();
			int[][] f = stones[i].getFaces();
			for (int j = 0, n = d.length; j < n; j++) {
				for (int u = 0; u < 3; u++) {
					int t = f[j][u];
					if (t == 3 || t == -3) {
						t = t < 0 ? 2 - u : 3 + u;
						int v = u == 2 ? 0 : u + 1;
						int w = u == 0 ? 2 : u - 1;
						faces[t][o[v] + 1][o[w] + 1] = d[j];
						break;
					}
				}
			}
		}
	}
	
	public int getState() {
		if(allComplete()) return AllDone;
		prime=onePageComplete();
		if(prime==null) {
			prime=oneCrossComplete();
			return (prime==null)?-1:FirstCross;
		}
		if(!middleComplete()) {
			return FirstLayer;
		}
		if(!topCrossComplete()) {
			return MiddleLayer;
		}
		if(!topFilled()) {
			return TopCross;
		}
		return cornerDone()?CornerDone:TopFilled;
	}
	
	
	
	private boolean middleComplete() {
		int u=norm<3?2-norm:norm-3;
		for(int i=0; i<6; i++) {
			if(i==norm||i==5-norm) continue;
			int v=i<3?2-i:i-3;
			int w=(v==2)?0:v+1;
			if(w==u) {
				if(faces[i][1][0]!=faces[i][1][1]||faces[i][1][2]!=faces[i][1][1]) {
					return false;
				}
			}
			else if(faces[i][0][1]!=faces[i][1][1]||faces[i][2][1]!=faces[i][1][1]) {
				return false;
			}
		}
		return true;
	}
	
	private boolean topCrossComplete() {
		int i=5-norm;
		int j=faces[i][1][1];
		return faces[i][0][1]==j&&faces[i][1][0]==j&&faces[i][2][1]==j&&faces[i][1][2]==j;
	}
	
	private boolean topFilled() {
		int f=5-norm;
		int v=faces[f][1][1];
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				if(faces[f][i][j]!=v) return false;
			}
		}
		return true;
	}
	
	private boolean cornerDone() {
		for(int i=0; i<6; i++) {
			if(i==norm||i==5-norm) continue;
			int f=faces[i][1][1];
			boolean t=false;
			for(int j=0; j<3; j++) {
				for(int k=0; k<3; k++) {
					if(faces[i][j][k]!=f) {
						if(t) return false;
						if(j!=1&&k!=1) {
							return false;
						}
						t=true;
					}
				}	
			}
		}
		return true;
	}
	
	private int[][][] fetchData() {
		if (data == null) {
			data = new int[26][3][3];
			for (int i = 0; i < 26; i++) {
				int[][] f = stones[i].getMatrix();
				for (int j = 0; j < 3; j++) {
					for (int k = 0; k < 3; k++) {
						data[i][j][k] = f[j][k];
					}
				}
			}
		}
		return data;
	}
	
	public boolean allComplete() {
		for(int i=0; i<6; i++) {
			int v=faces[i][0][0];
			for(int j=0; j<3; j++) {
				for(int k=0; k<3; k++) {
					if(faces[i][j][k]!=v) return false;
				}
			}
		}
		return true;
	}
	
	public int[][] onePageComplete() {
		LOOP:for(int i=0; i<6; i++) {
			int v=faces[i][0][0];
			for(int j=0; j<3; j++) {
				for(int k=0; k<3; k++) {
					if(faces[i][j][k]!=v) continue LOOP;
				}
			}
			fetchData();
			int[][][] s = Matrix.allMatrix();
			for(int j=0; j<32; j++) {
				for(int k=0; k<26; k++) {
					int[][] ss=data[k];
					int[][] tt=Matrix.product(s[j], ss);
					if(!Matrix.isSame(ss,tt)) {
						Matrix.print(ss);
						Matrix.print(tt);
						System.exit(1);
						
					}
					stones[k].setMatrix(Matrix.product(s[j], data[k]));
				}
				boolean b = onePageVerified(v);
				reset();
				if(b) {
					norm=i;
					return s[j];
				}
			}
		}
		return null;
	}
	
	public int[][] oneCrossComplete() {
		LOOP:for(int i=0; i<6; i++) {
			int v=faces[i][1][1];
			for(int j=0; j<4; j++) {
				int jx=(j==0)?0:(j==2)?2:1;
				int jy=(j==1)?1:(j==3)?2:1;
				if(faces[i][jx][jy]!=v) continue LOOP;
			}
			fetchData();
			int[][][] s = Matrix.allMatrix();
			for(int j=0; j<32; j++) {
				for(int k=0; k<26; k++) {
					stones[k].setMatrix(Matrix.product(s[j], data[k]));
				}
				boolean b = oneCrossVerified(v);
				reset();
				if(b) {
					norm=i;
					return s[j];
				}
			}
		}
		return null;
	}
	
	public int[][] getPrime() {
		return prime;
	}

	public int getNorm() {
		return norm;
	}

	private boolean onePageVerified(int type) {
		Point pnt=Matrix.getPoint(type);
		int axis=pnt.getAxis();
		int value=pnt.getValue();
		for(int i=0; i<26; i++) {
			int[] o=stones[i].coordinate();
			int[] types=stones[i].getIndexs();
			int n=types.length;
			if(n==1) {
				for(int j=0; j<3; j++) {
					if(o[j]!=0) {
						if(Matrix.getOrder(j,o[j])!=types[0]) return false;
						break;
					}
				}
			}
			else if(n==2){
				if(types[0]!=type&&types[1]!=type) {
					continue;
				}
				if(o[axis]!=value) return false;
				int in=types[0]==type?1:0;
				pnt=Matrix.getPoint(types[in]);
				if(stones[i].getFaces()[in][pnt.getAxis()]!=3*pnt.getValue()) return false;
			}
			else {
				if(types[0]!=type&&types[1]!=type&&types[2]!=type) {
					continue;
				}
				if(o[axis]!=value) return false;
				int pp=types[0]==type?0:types[1]==type?1:2;
				pp=pp==2?0:pp+1;
				pnt=Matrix.getPoint(types[pp]);
				if(o[pnt.getAxis()]!=pnt.getValue()) return false;
				pp=pp==2?0:pp+1;
				pnt=Matrix.getPoint(types[pp]);
				if(o[pnt.getAxis()]!=pnt.getValue()) return false;
				if(stones[i].getFaces()[pp][pnt.getAxis()]!=3*pnt.getValue()) return false;
			}	
		}
		return true;
	}
	
	
	private boolean oneCrossVerified(int type) {
		Point pnt=Matrix.getPoint(type);
		int axis=pnt.getAxis();
		int value=pnt.getValue();
		for(int i=0; i<26; i++) {
			int[] o=stones[i].coordinate();
			int[] types=stones[i].getIndexs();
			int n=types.length;
			if(n==1) {
				for(int j=0; j<3; j++) {
					if(o[j]!=0) {
						if(Matrix.getOrder(j,o[j])!=types[0]) return false;
						break;
					}
				}
			}
			else if(n==2){
				if(types[0]!=type&&types[1]!=type) {
					continue;
				}
				if(o[axis]!=value) return false;
				int in=types[0]==type?1:0;
				pnt=Matrix.getPoint(types[in]);
				if(stones[i].getFaces()[in][pnt.getAxis()]!=3*pnt.getValue()) return false;
			}
		}
		return true;
	}
	

	public void reset() {
		for (int i = 0; i < 26; i++) {
			int[][] f = new int[3][3];
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					f[j][k] = data[i][j][k];
				}
			}
			stones[i].setMatrix(f);
		}
	}

}
