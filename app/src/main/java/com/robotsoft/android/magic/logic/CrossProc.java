package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class CrossProc extends Worker {
	private static int[][] Neighbors = { { 1, 2, 4, 3 }, { 0, 2, 5, 3 },
			{ 0, 1, 4, 5 }, { 5, 4, 1, 0 },{ 3, 5, 2, 0 },{3,4,2,1} };
	private int root;

	public CrossProc(MoveSupport host, int root) {
		super(host);
		this.root=root;
	}

	public boolean process(int state) {
		if(state>0) {
			FirstLayer first=new FirstLayer(host,root);
			return first.process(state);
		}
		CenterOperator center = new CenterOperator(host);
		if (!center.process()) {
			System.out.println("Center failed");
			return false;
		}
		for (int i = 0; i < 4; i++) {
			if (!process(root, i)) {
				return false;
			}
		}
		return true;
	}

	private boolean centerSucceed() {
		for (int i = 0; i < 6; i++) {
			if(!centerSucceed(i))return false;
		}
		return true;
	}

	private boolean isOkay(int[] w, int k, int val) {
		switch (k) {
		case 0:
			return (w[2] == -val);
		case 1:
			return (w[1] == -val);
		case 2:
			return (w[0] == -val);
		case 3:
			return (w[0] == val);
		case 4:
			return (w[1] == val);
		case 5:
			return (w[2] == val);
		}
		return false;
	}

	private boolean edgeSuccess(int prime, int k) {
		int other=Neighbors[prime][k];
		Stone f = Matrix.findEdge(host.getStones(), prime, other);
		int[] w = f.getFaces()[f.getHint()];
		return isOkay(w, prime, 3) && isOkay(w, other, 2);
	}

	private boolean damage(int prime, int k) {
		if (!centerSucceed())
			return true;
		for (int i = 0; i < k; i++) {
			if (!edgeSuccess(prime, i))
				return true;
		}
		return false;
	}

	private boolean works(int prime, int k) {
		boolean b = !damage(prime, k) && edgeSuccess(prime, k);
		return b;
	}

	private boolean process(int prime, int k) {
		if (works(prime, k))
			return true;
		Point pnt=Matrix.getPoint(prime);
		int other = Neighbors[prime][k];
		int a0 = pnt.getAxis();
		int v0 = pnt.getValue();
		pnt=Matrix.getPoint(other);
		int a1 = pnt.getAxis();
		int v1 = pnt.getValue();
		int a2 = 3 - a0 - a1;
		Stone st = Matrix.findEdge(host.getStones(),prime, other);
		int[] w = st.coordinate();
		if (w[a1] == -v1) {
			move(a2, w[a2], true);
			move(a2, w[a2], true);
			if (damage(prime, k)) {
				move(a2, w[a2], false);
				move(a2, w[a2], false);
				move(a0, w[a0], true);
				move(a0, w[a0], true);
				if (damage(prime, k)) {
					move(a0, w[a0], false);
					move(a0, w[a0], false);
					boolean b = true;
					if (w[a0] == v0) {
						move(a1, w[a1], true);
						move(a1, w[a1], true);
					} else if (w[a0] == 0) {
						move(a1, w[a1], b);
						if (st.coordinate()[a0] == v0) {
							b = !b;
							move(a1, w[a1], b);
							move(a1, w[a1], b);
						}
					}
					move(a0, -v0, true);
					move(a0, -v0, true);
					if (damage(prime, k)) {
						move(a0, -v0, false);
						move(a0, -v0, false);
						if (w[a0] == 0) {
							move(a1, w[a1], !b);
						} else if (w[a0] == v0) {
							move(a1, w[a1], false);
							move(a1, w[a1], false);
						}
						if (w[a0] == 0) {
							b = true;
							move(a1, w[a1], b);
							if (st.coordinate()[a0] == v0) {
								b = !b;
								move(a1, w[a1], b);
								move(a1, w[a1], b);
							}
							move(a0, -v0, true);
							move(a0, -v0, true);
							move(a1, w[a1], !b);
							b = !damage(prime, k);
						} else
							b = false;
						if (!b) {
							System.out.println("Reverse Error");
							return false;
						}
					}
				}
			}
			if (works(prime, k))
				return true;
			w = st.coordinate();
		} else if (w[a1] == 0) {
			boolean tf = false;
			LL: for (int i = 0; i < 2; i++) {
				int o = (i == 0) ? a0 : a2;
				for (int j = 0; j < 2; j++) {
					boolean b = (j == 0);
					move(o, w[o], b);
					int[] nw = st.coordinate();
					if (nw[a1] != v1 || damage(prime, k)) {
						move(o, w[o], !b);
					} else {
						tf = true;
						break LL;
					}
				}
			}
			if (!tf) {
				move(a0, w[a0], true);
				move(a0, w[a0], true);
				int[] nw = st.coordinate();
				boolean b = false;
				move(a2, nw[a2], b);
				if (st.coordinate()[a1] != v1) {
					b = !b;
					move(a2, nw[a2], b);
					move(a2, nw[a2], b);
				}
				if (damage(prime, k)) {
					move(a2, nw[a2], !b);
					move(a0, w[a0], false);
					move(a0, w[a0], false);
					System.out.println("Cannot move to bottom from middle layer");
					return false;
				}
			}
			if (works(prime, k))
				return true;
			w = st.coordinate();
		}
		if (w[a0] == 0) {
			move(a1, v1, true);
			if (works(prime, k))
				return true;
			w = st.coordinate();
		}
		if (w[a0] == v0) {
			move(a1, v1, true);
			move(a1, v1, true);
		} else if (w[a0] == -v0) {
			move(a1, v1, true);
			move(a1, v1, true);
			if (works(prime, k))
				return true;
			move(a1, v1, false);
			move(a1, v1, false);
		}
		for (int i = 0; i < 2; i++) {
			boolean b1 = (i == 0);
			for (int j = 0; j < 2; j++) {
				boolean b2 = (j == 0);
				move(a1, v1, b1);
				move(a0, 0, b2);
				move(a1, v1, !b1);
				move(a0, 0, !b2);
				w = st.coordinate();
				if (works(prime, k))
					return true;
				move(a0, 0, b2);
				move(a1, v1, b1);
				move(a0, 0, !b2);
				move(a1, v1, !b1);
			}
		}
		System.out.println("Cannot Rotate");
		return false;
	}

}
