package com.robotsoft.android.magic;

import java.util.ArrayList;
import java.util.List;

import com.robotsoft.android.magic.logic.Matrix;



public class CubeFace {
	private MatrixRot rot;
	private int order;
	private int sign;
	private int[][] points;
	private double prvv,prvw;
	private boolean dragged=true;
	private int[][] faces=new int[9][3];
	private int[] index;
	
	public CubeFace(MatrixRot rot, int order, int sign) {
		this.rot=rot;
		this.order=order;
		this.sign = sign;
		int u=(order==2)?0:order+1;
		int v=(order==0)?2:order-1;
		points=new int[3][3];
		for(int i=0; i<3; i++) {
			points[order][i]=sign;
		}
		points[u][0]=-1;
		points[v][0]=-1;
		points[u][1]=1;
		points[v][1]=-1;
		points[u][2]=-1;
		points[v][2]=1;
		index=new int[9];
		
		for(int i=0; i<9; i++) {
			int[] w=new int[3];
			w[order]=3*sign;
			w[u]=2*(i%3-1);
			w[v]=2*(i/3-1);
			faces[i]=w;
			index[i]=sign==-1?2-order:3+order;
		}
	}
	
	public Square middlePlane(int s, double[][] mat) {
		int[][] t=new int[3][3];
		int u=(order==2)?0:order+1;
		int v=(order==0)?2:order-1;
		for(int i=0; i<3; i++) {
		  t[order][i]=s;
		  t[u][i]=points[u][i]*3;
		  t[v][i]=points[v][i]*3;
		}
		double[][] d = new double[2][3];
		for (int j = 0; j < 3; j++) {
			d[0][j] = mat[0][0] * t[0][j] + mat[0][1] * t[1][j] + mat[0][2] * t[2][j];
			d[1][j] = mat[1][0] * t[0][j] + mat[1][1] * t[1][j] + mat[1][2] * t[2][j];
		}
		double[][] f=new double[4][2];
		f[0][0]=d[0][0];
		f[0][1]=d[1][0];
		f[1][0]=d[0][1];
		f[1][1]=d[1][1];
		f[3][0]=d[0][2];
		f[3][1]=d[1][2];
		f[2][0]=d[0][1]+d[0][2]-d[0][0];
		f[2][1]=d[1][1]+d[1][2]-d[1][0];
		return new Square(6,f); 
	}
	
	public int[] getFace(int x, int y, int z) {
		int[] p=new int[]{x,y,z};
		if(p[order]!=sign) return null;
		int u=(order==2)?0:order+1;
		int v=(order==0)?2:order-1;
		int i=p[u]+1+3*(p[v]+1);
		return faces[i];
	}
	
	public int getIndex() {
		return index[0];
	}
	
	public boolean isVisible() {
		return rot.visible(order, sign);
	}
	
	public int findRotation(int dx, int dy) {
		if(prvv<-1) {
			return -1;
		}
		int v=order==2?0:order+1;
		int w=order==0?2:order-1;
		double[][] z=rot.getMatrix();
		double a11=z[0][v];
		double a12=z[0][w];
		double a21=z[1][v];
		double a22=z[1][w];
		double dv=dx*a22-dy*a12;
        double dw=dy*a11-dx*a21;
		double av=Math.abs(dv);
		double aw=Math.abs(dw);
		int axis;
		int pos;
		int dir;
		double d=a11*a22-a12*a21;
		if(av>aw) {
			pos=(prvw<-0.33)?-1:(prvw>0.33)?1:0;
			axis=order==0?2:order-1;
			dir=1;
			if(sign==-1)dir=-dir;
			if((dv>=0)!=(d>=0)) dir=-dir;
		}
		else {
			pos=(prvv<-0.33)?-1:(prvv>0.33)?1:0;
			axis = order==2?0:order+1;
			dir=-1;
			if(sign==-1)dir=-dir;
			if( (dw>=0) != (d>=0) ) dir=-dir;
		}	
		dragged=true;
		prvv=-2;
		return Matrix.getAction(axis, pos, dir==1);
	}
	

	public boolean isDragged() {
		return dragged;
	}

	public void setDragged(boolean dragged) {
		this.dragged = dragged;
	}

	
	boolean contains(double x, double y) {
		if(!rot.visible(order,sign)) return false;
		double[] u = reverse(x, y);
		boolean t=u[0]>-1&&u[0]<1&&u[1]>-1&&u[1]<1;
		if(t) {
		    prvv=u[0];
		    prvw=u[1];
		}
		else {
			prvv=-2;
		}
		dragged=false;
		return t;
	}
	
	
	double[] reverse(double x, double y) {	
		int v=order==2?0:order+1;
		int w=order==0?2:order-1;
		double[][] z=rot.getMatrix();
		double a11=z[0][v];
		double a12=z[0][w];
		double a21=z[1][v];
		double a22=z[1][w];
		double f1=x-sign*z[0][order];
		double f2=y-sign*z[1][order];
		double d=a11*a22-a12*a21;
		if(d==0) return null;
		double a=(f1*a22-f2*a12)/d;
		double b=(f2*a11-f1*a21)/d;
		return new double[]{a,b};
	}
	
	public void resetIndex(int[][] values) {
		for(int i=0; i<9; i++) {
			for(int j=0; j<54; j++) {
				if(faces[i][0]==values[j][0]&&faces[i][1]==values[j][1]&&faces[i][2]==values[j][2]) {
					index[i]=values[j][3];
					break;
				}
			}
		}
	}
	
	
	public List<Square> getSquares(int axis, int value, double[][] mat) {
		if(mat[2][order]*sign<=0) return null;
		boolean normal=(order==axis);
		if(normal&&sign!=value) {
			return null;
		}
		double[][] values = new double[2][3];
		for (int j = 0; j < 3; j++) {
			values[0][j] = mat[0][0] * points[0][j] + mat[0][1] * points[1][j]
					+ mat[0][2] * points[2][j];
			values[1][j] = mat[1][0] * points[0][j] + mat[1][1] * points[1][j]
			        + mat[1][2] * points[2][j];
		}
		List<Square> list=new ArrayList<Square>();
		double ax = (values[0][1]-values[0][0])/3;
		double ay = (values[1][1]-values[1][0])/3;
		double bx = (values[0][2]-values[0][0])/3;
		double by = (values[1][2]-values[1][0])/3;
		double[][][] f=new double[4][4][2];
		for(int i=0; i<=3; i++) for(int j=0; j<=3; j++) {
			f[i][j][0]=i*ax+j*bx+values[0][0];
			f[i][j][1]=i*ay+j*by+values[1][0];
		}
		int w=order==2?0:order+1;
		boolean first=(w==axis);
		for(int i=0; i<9; i++) {
		   int u=i%3;
		   int v=i/3;
		   if(axis>=0 && !normal) {
			   if(first) {
				  if(u-1!=value) continue; 
			   }
			   else {
				   if(v-1!=value) continue;
			   }
		   }
		   double[][] res=new double[4][2];
		   res[0][0]=f[u][v][0]*3;
		   res[0][1]=f[u][v][1]*3;
		   res[1][0]=f[u+1][v][0]*3;
		   res[1][1]=f[u+1][v][1]*3;
		   res[2][0]=f[u+1][v+1][0]*3;
		   res[2][1]=f[u+1][v+1][1]*3;
		   res[3][0]=f[u][v+1][0]*3;
		   res[3][1]=f[u][v+1][1]*3;
			list.add(new Square(index[i], res));
		}
		return list;
	}
	
	public double[][][] getMiddlePoints(int axis, int value, double[][] mat) {
		if (mat[2][order] * sign <= 0)
			return null;
		boolean normal = (order == axis);
		if (normal && sign != value) {
			return null;
		}
		double[][] values = new double[2][3];
		for (int j = 0; j < 3; j++) {
			values[0][j] = mat[0][0] * points[0][j] + mat[0][1] * points[1][j]
					+ mat[0][2] * points[2][j];
			values[1][j] = mat[1][0] * points[0][j] + mat[1][1] * points[1][j]
					+ mat[1][2] * points[2][j];
		}
		double ax = (values[0][1] - values[0][0]) / 3;
		double ay = (values[1][1] - values[1][0]) / 3;
		double bx = (values[0][2] - values[0][0]) / 3;
		double by = (values[1][2] - values[1][0]) / 3;
		if (Math.abs(ax * by - ay * bx) < 0.08)
			return null;
		if (Math.abs(ay * bx) > Math.abs(ax * by)) {
			double f = ax;
			ax = bx;
			bx = f;
			f = ay;
			ay = by;
			by = f;
		}
		double[][][] f = new double[4][4][2];
		for(int i = 0; i <= 3; i++) {
			int ii = (ax > 0) ? i : 3 - i;
			for (int j = 0; j <= 3; j++) {
				int jj = (by > 0) ? j : 3 - j;
				f[ii][jj][0] = i * ax + j * bx + values[0][0];
				f[ii][jj][1] = i * ay + j * by + values[1][0];
			}
		}
		double[][][] g = new double[3][3][2];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				g[i][j][0] = (f[i][j][0] + f[i + 1][j + 1][0])*3 / 2;
				g[i][j][1] = (f[i][j][1] + f[i + 1][j + 1][1])*3 / 2;
			}
		}
		return g;
	}
	
	public String toString() {
		String s=((order==0)?"x":(order==1)?"y":"z")+"="+sign;
		if(!dragged) {
			int v=prvv<-0.3?-1:prvv>0.3?1:0;
			int w=prvw<-0.3?-1:prvw>0.3?1:0;
			s+=" Selected at ("+v+","+w+")";
		}
	    return s;	
	}

}
