package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class FirstLayer extends Worker {
	private static int[][][] Corners = {
		{ {1, 2}, {2,4},{3,4} ,{1,3} },
		{ {0, 2}, {0,3},{3,5} ,{2,5} },
		{ {0, 1}, {1,5},{5,4} ,{4,0} },
		{ {0, 1}, {1,5},{5,4} ,{4,0} },
		{ {0, 2}, {0,3},{3,5} ,{2,5} },
		{ {1, 2}, {2,4},{3,4} ,{1,3} }
	};
	private int root;
	
	public FirstLayer(MoveSupport host,int root) {
		super(host);
		this.root=root;
	
	}
	
	public boolean process(int state) {
		if(state>1) {
			MiddleLayer middle=new MiddleLayer(host,root);
			return middle.process(state);
		}
		if(state<1) {
			CrossProc edge = new CrossProc(host, root );
			boolean b=edge.process();
			if(!b) {
				System.out.println("Edge failed");
				return false;
			}
			if(state!=-1) {
				return true;
			}
		}
		for(int i=0; i<4; i++) processCorner(root, i);
		return true;
	}
	
	private boolean processCorner(int prime, int k) {
		Point pnt=Matrix.getPoint(Corners[prime][k][0]);
		int a0=pnt.getAxis();
		int v0=pnt.getValue();
		pnt=Matrix.getPoint(Corners[prime][k][1]);
		int a1=pnt.getAxis();
		int v1=pnt.getValue();
		Stone st=Matrix.findCorner(host.getStones(), prime, Corners[prime][k][0], Corners[prime][k][1]);
		int[] w=st.coordinate();
		int norm = (prime == 2 || prime == 3) ? 0 : (prime == 1 || prime == 4) ? 1 : 2;
		int base = prime < 3 ? -1 : 1;
		if(w[norm]==base) {
			if(w[a0]==v0&&w[a1]==v1) {
				int[] z=st.getFaces()[st.getHint()];
				if(z[norm]==3*base) return true;
			}
			boolean b=true;
			move(a1,w[a1],b);
			if(st.coordinate()[norm]==base) {
				b=!b;
				move(a1,w[a1],b);
				move(a1,w[a1],b);
			}
			move(norm,-base,true);
			move(a1,w[a1],!b);
			w=st.coordinate();
		}
		if(w[a0]!=v0||w[a1]!=v1) {
			for(int i=0; i<3; i++) {
				move(norm,-base,true);
				w=st.coordinate();
				if(w[a0]==v0&&w[a1]==v1) {
					break;
				}
			}
		}
		doMove(st, norm, a0, a1, base, v0, v1);
		return false;
	}
	
	
	private void doMove(Stone st, int a0, int a1, int a2, int v0, int v1,int v2) {
		int[] z=st.getFaces()[st.getHint()];
		if(z[a1]==3*v1) {
			doProc(st, a1, v1, a0, v0);
		}
		else if(z[a2]==3*v2){
			doProc(st, a2, v2, a0, v0);
		}
		else {
			boolean b=true;
			move(a1,v1,b);
			if(st.coordinate()[a0]!=-v0) {
				b=!b;
				move(a1,v1,b);
				move(a1,v1,b);
			}
			move(a0,-v0,true);
			move(a0,-v0,true);
			move(a1,v1,!b);
			move(a0,-v0,false);
			if(st.coordinate()[a1]!=v1) {
				move(a0,-v0,true);
				move(a0,-v0,true);
			}
			doProc(st, a1, v1, a0, v0);
		}
	}
	
	private void doProc(Stone st, int index, int value, int norm, int base) {
		boolean b=false;
		move(index,value,b);
		if(st.coordinate()[norm]!=-base) {
			b=!b;
			move(index,value,b);
			move(index,value,b);
		}
		move(norm,-base,false);
		if(st.coordinate()[index]!=value) {
			move(norm,-base,true);
			move(norm,-base,true);
		}
		move(index,value,!b);
	}

}
