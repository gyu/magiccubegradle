package com.robotsoft.android.magic;

public class DBConstants {
	public static final String ALPHA="alpha";
	public static final String BETA="beta";
	public static final String TIME="time";
	public static final String MOVE_NUMBER="move_number";
	public static final String COLOR="color";
	public static final String MUSIC="music";
	public static final String SHUFFLES="shuffles";
	public static final String OPERATIONS="operations";
	
}
