package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class CenterOperator extends Worker {
	
	
	public CenterOperator(MoveSupport host) {
		super(host);
	}

	public boolean process(int state) {
		if(state!=-1) return true;
		for(int i=0; i<6; i++) {
			if(!move(i)) return false;
		}
		return true;
	}
	
	private boolean doDamage(int index) {
		for(int i=0; i<index; i++) {
			if(!centerSucceed(i)) return true;
		}
		return false;
	}
	
	
	private boolean move(int index) {
		Point pnt=Matrix.getPoint(index);
		Stone stone=Matrix.findCenter(host.getStones(), index);
		int[] xyz=stone.coordinate();
		int base=0;
		for(int i=1; i<3; i++) {
			if(xyz[i]!=0) {
				base=i;
				break;
			}
		}
		if(base==pnt.getAxis()) {
			if(xyz[base]!=pnt.getValue()) {
				int u=base==2?0:base+1;
				int v=base==0?2:base-1;
				move(u,0,true);
				move(u,0,true);
				if(doDamage(index)) {
					move(u,0,false);
					move(u,0,false);
					move(v,0,true);
					move(v,0,true);
					if(doDamage(index)) {
						move(v,0,false);
						move(v,0,false);
						return false;
					}
				}
			}
			return true;
		}
		int u=base==2?0:base+1;
		if(u==pnt.getAxis()) {
			u=base==0?2:base-1;
		}
		move(u,0,true);
		xyz=stone.coordinate();
		if(xyz[pnt.getAxis()]!=pnt.getValue()) {
			move(u,0,false);
			move(u,0,false);
			if(doDamage(index)) {
				move(u,0,true);
				return false;
			}
		}
		else if(doDamage(index)) {
			move(u,0,false);
			return false;
		}
		return true;
	}

}
