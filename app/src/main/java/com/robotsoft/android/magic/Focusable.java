package com.robotsoft.android.magic;

import android.graphics.Canvas;

public interface Focusable {
	boolean lostFocus(int dx, int dy);
	boolean gainFocus(boolean dir, int index);
	boolean hasFocus();
	int mouseUp();
	void paint(Canvas g);
	boolean isDown();
	boolean in(int x,int y, int range);
	void setSelected(boolean b);
}
