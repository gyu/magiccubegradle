package com.robotsoft.android.magic.logic;

import com.robotsoft.android.magic.Stone;

public class MiddleLayer extends Above {
	
	public MiddleLayer(MoveSupport host, int root) {
		super(host, root);
	}
	
	public boolean process(int state) {
		if(state>2) {
			TopLayer top=new TopLayer(host, root);  
			return top.process(state);
		}
		if(state<2) {
			FirstLayer layer= new FirstLayer(host, root );
			boolean b=layer.process(state);
			if(!b) {
				System.out.println("Corner failed");
				return false;
			}
			if(state!=-1) return true;
		}
		for(int i=0; i<4; i++) {
			if(!processCube(root, i))return false;
		}
		return true;
	}

	
	private boolean edgeOkay(Stone s) {
		int[] w=s.coordinate();
		if(w[za]!=0) return false;
		int[] types=s.getIndexs();
		Point p=null;
		for(int i=0; i<2; i++) {
			p=Matrix.getPoint(types[i]);
			if(w[p.getAxis()]!=p.getValue()) return false;
		}
		int[] f=s.getFaces()[1];
		if(f[p.getAxis()]!=3*p.getValue()) return false;
		return true;
	}
	
	private boolean processCube(int prime, int inx) {
		Stone st=Matrix.findEdge(host.getStones(), Cubes[prime][inx][0], Cubes[prime][inx][1]);
		face=Cubes[root][inx][0];
		right=Cubes[root][inx][1];
		fix();
		if(edgeOkay(st)) {
			return true;
		}
		int[] w=st.coordinate();
		int[] in=st.getIndexs();
		int type=-1;
		if(w[za]==-zv) {
			for(int i=0; i<4; i++) {
				if(w[xa]==xv) {
					int q=(in[0]==face)?0:1;
					int[] qs=st.getFaces()[q];
					if(qs[xa]==3*xv) {
						type=0;
						break;
					}
				}
				if(w[ya]==yv) {
					int q=(in[0]==right)?0:1;
					int[] qs=st.getFaces()[q];
					if(qs[ya]==3*yv) {
						type=1;
						break;
					}
				}
				move(za,-zv,true);
				w=st.coordinate();
			}
		}
		else {
		    int[] t=st.getFaces()[0];
		    int t1=-1;
		    int t2=-1;
		    for(int i=0; i<2; i++) {
		    	if(t[i]!=0) {
		    		int v=(2-i);
		    		if(t[i]>0)v=5-v;
		    		if(t1==-1) {
		    			t1=v;
		    		}
		    		else {
		    			t2=v;
		    		}
		    	}
		    }
		    for(int i=0; i<4; i++) {
		    	if(Cubes[root][i][0]==t1&&Cubes[root][i][1]==t2) {
		    		face=t1;
		    		right=t2;
		    		break;
		    	}
		    	if(Cubes[root][i][0]==t2&&Cubes[root][i][1]==t1) {
		    		face=t2;
		    		right=t1;
		    		break;
		    	}
		    	
		    }
		    fix();
			f54212356();
		    return processCube(prime,  inx);
		}
	    if(type==1) {
	    	f2();
	    	f3();
	    	f5();
	    	f6();
	    	f5();
	    	f4();
	    	f2();
	    	f1();
	    }
	    else if(type==0) {
	    	f54212356();
	    }
	    else {
	    	System.out.println("Check More for "+Cubes[prime][inx][0]+","+ Cubes[prime][inx][1]);
	    	return false;
	    }
	    return true;

	}

	
	private void f54212356() {
	   	f5();
    	f4();
    	f2();
    	f1();
    	f2();
    	f3();
    	f5();
    	f6();
 	}
	
}
